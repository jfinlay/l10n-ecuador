Localización Ecuatoriana
========================

La localización ecuatoriana pretende ser una base sólida y bien estructurada sobre la cual, usuarios finales y proveedores de servicios, puedan implementar sus soluciones, sea para su uso privado o para brindar el servicio a otras empresas.

Alcance.
========
La presente localización pretende implementar las siguienes soluciones:

Cuentas contables.
------------------
- Plan de cuentas contables NIIF Pymes, de acuerdo al catálogo de la Superintendencia de compañías.
- Cuentas contables no tipificadas en el catalogo, pero de uso generalizado (cuentas para impuestos, IESS, etc)
- Cuentas contables específicas a diferentes giros de negocio, de uso generalizado.

Impuestos.
----------
- Declaración de impuestos comunes para empresas cuyo giro de negiocio principal es local.
- Declaración de impuestos específicos de empresas que realizan comercio exterior.
- Generación del Anexo Transaccional (permite la declaración de los formularios 103 y 104.

Recursos humanos.
-----------------

- Rol de pagos
- Provisiones
- Manejo de anticipos y prestamos IESS

Herramientas generales.
-----------------------
- Herramienta de configuración sencilla para la configuración inicial.
