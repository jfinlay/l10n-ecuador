# -*- coding: utf-8 -*-
{
    'name': "SRI - Advanced tax computation",
    'summary': """Alows to declare the ATS based on lines instead of invoices.""",
    'version': '9.0.1.0.0',
    'author': "Fabrica de Software Libre,Odoo Community Association (OCA)",
    'maintainer': 'Fabrica de Software Libre',
    'website': 'http://www.libre.ec',
    'license': 'AGPL-3',
    'category': 'Account',
    'depends': [
        'account',
        'l10n_ec_sri',
    ],
    'data': [
        'views/account_invoice.xml',
        'views/sri.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
}
