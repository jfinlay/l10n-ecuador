# -*- coding: utf-8 -*-
from openerp import models, fields, api

class SriAtsLine(models.Model):
    _name = 'l10n_ec_sri.ats.line'

    invoice_id = fields.Many2one(
        'account.invoice', ondelete='cascade', string="Invoice", )

    detalleair_ids = fields.One2many(
        'l10n_ec_sri.detalleair', inverse_name='sri_ats_line_id', string="Detalle AIR", )

    codsustento = fields.Char('codSustento', )
    basenograiva = fields.Float('baseNoGraIva', digits=(9, 2), )
    baseimponible = fields.Float('baseImponible', digits=(9, 2), )
    baseimpgrav = fields.Float('baseImpGrav', digits=(9, 2), )
    baseimpexe = fields.Float('baseImpExe', digits=(9, 2), )
    montoice = fields.Float('montoIce', digits=(9, 2), )
    montoiva = fields.Float('montoIva', digits=(9, 2), )
    valretbien10 = fields.Float('valRetBien10', digits=(9, 2), )
    valretserv20 = fields.Float('valRetServ20', digits=(9, 2), )
    valretserv50 = fields.Float('valRetServ50', digits=(9, 2), )
    valorretbienes = fields.Float('valorRetBienes', digits=(9, 2), )
    valorretservicios = fields.Float('valorRetServicios', digits=(9, 2), )
    valretserv100 = fields.Float('valRetServ100', digits=(9, 2), )


class DetalleAir(models.Model):
    _name = 'l10n_ec_sri.detalleair'

    sri_ats_line_id = fields.Many2one(
        'l10n_ec_sri.ats.line', ondelete='cascade', string="ATS Line", )

    codretair = fields.Char('codRetAir')
    baseimpair = fields.Char('baseImpAir')
    porcentajeair = fields.Char('porcentajeAir')
    valretair = fields.Float('valRetAir', digits=(9, 2),)


class SriTaxLine(models.Model):
    _name = 'l10n_ec_sri.tax.line'

    month = fields.Selection([('01', 'Enero'),
                             ('02', 'Febrero'),
                             ('03', 'Marzo'),
                             ('04', 'Abril'),
                             ('05', 'Mayo'),
                             ('06', 'Junio'),
                             ('07', 'Julio'),
                             ('08', 'Agosto'),
                             ('09', 'Septiembre'),
                             ('10', 'Octubre'),
                             ('11', 'Noviembre'),
                             ('12', 'Diciembre')
                             ],
                            string='Month', )

    year = fields.Char('Year', )

    invoice_line_id = fields.Many2one(
        'account.invoice.line', ondelete='cascade', string="Invoice line", )

    formulario = fields.Char('Formulario', )
    campo = fields.Char('Campo', )
    group = fields.Char('SRI Group', )
    amount = fields.Float('Valor del impuesto', digits=(9, 2), )
    porcentaje = fields.Char('Porcentaje', )
    code = fields.Char('Código', )
    base = fields.Float('Base del impuesto', digits=(9, 2), )
    gap = fields.Float('Diferencia', digits=(9, 2), )
