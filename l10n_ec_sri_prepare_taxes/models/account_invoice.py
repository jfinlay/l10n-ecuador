# -*- coding: utf-8 -*-
from openerp import models, fields, api

class AccountInvoice(models.Model):
    _inherit = ['account.invoice']

    codsustento = fields.Char('Sustento Tributario', )
    sri_ats_line_ids = fields.One2many(
        'l10n_ec_sri.ats.line', inverse_name='invoice_id', string="Línea de ATS", )

    @api.multi
    def button_prepare_sri_declaration(self):
        for inv in self:
            # Aplica en compras y ventas.
            inv.get_sri_tax_lines()

            # Aplicar solo en compras.
            if inv.type in ('in_refund','in_invoice'):
                inv.get_codsustento()
                inv.get_sri_cero_air()
                inv.get_sri_ats_lines()

    @api.multi
    def get_codsustento(self):
        for inv in self:
            sustentos = set([])
            for line in inv.invoice_line_ids:
                tax_sustento = line.invoice_line_tax_ids.filtered(lambda x: x.sustento_id)
                if tax_sustento:
                    codsustento = tax_sustento.sustento_id.code
                    sustentos.add(codsustento)
                    line.write({'codsustento': codsustento})

            # Convierte el set en una secuencia que se pueda dividir con split desde un char.
            str_sustentos = ''
            for s in sustentos:
                str_sustentos += s + ' '

            inv.write({'codsustento': str_sustentos})

    @api.multi
    def get_sri_tax_lines(self):
        for inv in self:
            year = inv.date_invoice[0:4]
            month = inv.date_invoice[5:7]

            print 'ANIO', year
            print 'MES', month

            # Datos para crear los impuestos en las líneas
            sri_tax_lines = []

            # Los impuestos de la factura, en diccionario.
            inv_taxes = []

            # Obtenemos la información de los impuestos de la factura para hacer el cuadre.
            for tax in inv.tax_line_ids:
                # Calcula en cuántas líneas es utilizado el impuesto.
                lines = inv.invoice_line_ids
                nro = 0
                for line in lines:
                    for lt in line.invoice_line_tax_ids:
                        if lt == tax:
                            nro += 1

                # Detecta el formulario desde los tag.
                # TODO: ¿Cambiar los tag por dos campos en account.tax para evitar calcular estos datos.?
                formulario = ''
                campo = ''
                for tag in tax.tax_id.tag_ids:
                    tag_list = tag.name.split("_")
                    if tag_list[0] in ('101', '103', '104'):
                        formulario = tag_list[0]
                        campo = tag_list[1]

                # Genera un diccionario con los impuestos de la factura.
                inv_taxes.append({
                    'id': tax.tax_id.id,
                    'formulario': formulario,
                    'campo': campo,
                    'amount': abs(tax.amount),
                    'base': tax.tax_base,
                    'code': tax.tax_id.sri_code,
                    'group': tax.tax_id.tax_group_id.name,
                    'porcentaje': str(abs(int(tax.tax_id.amount))) or '0',
                    'nro': nro,
                })

            # Ordena las líneas de menor a mayor para aplicar la diferencia a la línea con mayor valor.
            lines = inv.invoice_line_ids.sorted(lambda x: x.price_subtotal)

            for line in lines:
                # Limpia líneas de impuestos anteriormente calculadas
                line.sri_tax_line_ids.unlink()

                # Prepara los parámetros para el cálculo de impuestos.
                currency = line.invoice_id and line.invoice_id.currency_id or None
                price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)

                # Calcula el valor de los impuestos por línea
                tax_obj = line.invoice_line_tax_ids.compute_all(
                    price, currency, line.quantity, product=line.product_id,
                    partner=inv.partner_id)

                # Selecciona los impuestos de la línea en diccionario.
                taxes = tax_obj['taxes']

                # Calculamos el valor del impuesto en cada línea.
                gap = 0
                # Repasamos cada impuesto calculado en los impuestos de la línea.

                for lt in taxes:
                    for t in inv_taxes:
                        if lt['id'] == t['id']:
                            # Si es la última línea en la que se usa el impuesto.
                            # Usamos como impuesto el valor restante, no el calculado.
                            # Registramos la diferencia en el campo gap para revisión.
                            if t['nro'] == 1:
                                if lt['amount'] != t['amount']:
                                    gap = t['amount'] - lt['amount']
                                line_amount = t['amount']
                            else:
                                line_amount = lt['amount']

                            # Agregamos el impuesto a un diccionario que se usará para crear las líneas de impuestos.
                            sri_tax_lines.append({
                                'invoice_line_id': line.id,
                                'year': year,
                                'month': month,
                                'formulario': t['formulario'],
                                'campo': t['campo'],
                                'group': t['group'],
                                'amount': abs(line_amount),
                                'base': lt['tax_base'],
                                'porcentaje': t['porcentaje'],
                                'code': t['code'],
                                'gap': gap,
                            })

                            # Restamos el valor del impuesto de la línea del de la factura.
                            t['amount'] -= line_amount
                            # Reducimos la línea en uno, para idenfificar la última línea que usa el impuesto.
                            t['nro'] -= 1

                # Obtenemos los id de todos los impuestos con valor.
                t_ids = []
                for t in inv.tax_line_ids:
                    t_ids.append(t.tax_id.id)

                # Repasamos todos los impuestos en búsqueda de impuestos en cero.
                for tax in line.invoice_line_tax_ids:

                    # Obtenemos el formulario y el campo desde los tags.
                    # TODO: Hacer una formula separada y llamarla para unificar con el uso anterior.
                    for tag in tax.tag_ids:
                        tag_list = (tag.name).split("_")
                        if tag_list[0] in ('101', '103', '104'):
                            formulario = tag_list[0]
                            campo = tag_list[1]

                    if tax.id not in t_ids:
                        # Si no están en la lista de impuestos con valor
                        # Los agregamos con valor cero y base igual al price_subtotal de la línea.

                        sri_tax_lines.append({
                            'invoice_line_id': line.id,
                            'year': year,
                            'month': month,
                            'formulario': formulario,
                            'campo': campo,
                            'group': tax.tax_group_id.name,
                            'amount': 0.0,
                            'base': line.price_subtotal,
                            'porcentaje': str(abs(int(tax.amount))) or '0',
                            'code': tax.sri_code,
                        })

            for l in sri_tax_lines:
                self.env['l10n_ec_sri.tax.line'].create(l)

    @api.multi
    def get_sri_cero_air(self):
        for inv in self:
            # TODO usar .strftime("%Y") en lugar de [:]
            # para evitar errores con fechas almacendas en otros formatos
            year = inv.date_invoice[0:4]
            month = inv.date_invoice[5:7]
            for line in inv.invoice_line_ids:
                base = sum(t.base for t in line.sri_tax_line_ids if t.group == 'RetIr')
                residual = line.price_subtotal - base
                if round(residual, 2) > 0:

                    self.env['l10n_ec_sri.tax.line'].create({
                        'invoice_line_id': line.id,
                        'year': year,
                        'month': month,
                        'formulario': '103',
                        'campo': '332',
                        'group': 'RetIr',
                        'amount': 0.0,
                        'base': residual,
                        'porcentaje': '0',
                        'code': '332',
                    })

    @api.multi
    def get_sri_ats_lines(self):
        for inv in self:
            # Limpia líneas de ATS anteriormente calculadas
            inv.sri_ats_line_ids.unlink()

            # Hacemos una lista de los sustentos de la factura.
            sustentos = inv.codsustento.split()

            # Diccionario para crear la línea de ATS en la factura.
            sri_ats_lines = []

            for s in sustentos:
                basenograiva = 0.0
                baseimponible = 0.0
                baseimpgrav = 0.0
                baseimpexe = 0.0
                montoice = 0.0
                montoiva = 0.0
                valretbien10 = 0.0
                valretserv20 = 0.0
                valretserv50 = 0.0
                valorretbienes = 0.0
                valorretservicios = 0.0
                valretserv100 = 0.0

                detalleair = []

                for line in inv.invoice_line_ids:
                    if line.codsustento == s:
                        for tl in line.sri_tax_line_ids:
                            # AGREGAMOS LAS BASES DE IMPUESTO SEGÚN CORRESPONDE.
                            if tl.group == 'NoGraIva':
                                basenograiva += tl.base
                            elif tl.group == 'Imponible':
                                baseimponible += tl.base
                            elif tl.group == 'ImpGrav':
                                baseimpgrav += tl.base
                                # Solamente el grupo ImpGrav genera valores en montoiva
                                montoiva += tl.amount
                            elif tl.group == 'ImpExe':
                                baseimpexe += tl.base
                            # AGREGAMOS LOS VALORES DEL IMPUESTO DE LAS RETENCIONES.
                            elif tl.group == 'RetBien10':
                                valretbien10 += tl.amount
                            elif tl.group == 'RetServ20':
                                valretserv20 += tl.amount
                            elif tl.group == 'RetServ50':
                                valretserv50 += tl.amount
                            elif tl.group == 'RetBienes':
                                valorretbienes += tl.amount
                            elif tl.group == 'RetServicios':
                                valorretservicios += tl.amount
                            elif tl.group == 'RetServ100':
                                valretserv100 += tl.amount
                            # AGREGAMOS EL VALOR DEL ICE.
                            elif tl.group == 'Ice':
                                montoice += tl.amount

                            # HACEMOS LOS DICCIONARIOS DE RETENCIONES DE IR.
                            # TODO Renombrar el grupo RetIr por RetAir para ajustarnos a la terminología del ATS.
                            elif tl.group == 'RetIr':
                                # Buscamos una línea de retención con el mismo código.
                                air = next((item for item in detalleair if item["codretair"] == tl.code), False)

                                if not air:
                                    # Agregamos el diccionario, no se agrega directamente con 0,0 porque
                                    # al hacerlo, falla la búsqueda anterior.
                                    detalleair.append({
                                        'valretair': abs(tl.amount),
                                        'baseimpair': tl.base,
                                        'codretair': tl.code,
                                        'porcentajeair': tl.porcentaje,
                                    })
                                else:
                                    air['baseimpair'] += tl.base
                                    air['valretair'] += abs(tl.amount)

                # Agregamos 0,0 a la lista para que Odoo cree las líneas.
                detalleair_line = []
                for air in detalleair:
                    detalleair_line.append((0,0, air))

                sri_ats_lines.append({
                    'invoice_id': inv.id,
                    'codsustento': s,
                    'basenograiva': basenograiva,
                    'baseimponible': baseimponible,
                    'baseimpgrav': baseimpgrav,
                    'baseimpexe': baseimpexe,
                    'montoice': montoice,
                    'montoiva': montoiva,
                    'valretbien10': abs(valretbien10),
                    'valretserv20':abs(valretserv20),
                    'valretserv50': abs(valretserv50),
                    'valorretbienes': abs(valorretbienes),
                    'valorretservicios': abs(valorretservicios),
                    'valretserv100': abs(valretserv100),
                    'detalleair_ids': detalleair_line,
                })

            for l in sri_ats_lines:
                self.env['l10n_ec_sri.ats.line'].create(l)

class AccountInvoiceLine(models.Model):
    _inherit = ['account.invoice.line']

    sri_tax_line_ids = fields.One2many(
        'l10n_ec_sri.tax.line', inverse_name='invoice_line_id', string="Impuestos a declarar", )

    codsustento = fields.Char('Sustento tributario', )
