# -*- coding: utf-8 -*-
###############################################
# For copyright and license notices, see __openerp__.py file in module root
# directory
###############################################

from openerp import models, fields, api

class EcuadorConfigSettings(models.TransientModel):
    _name = 'l10n_ec_installer.config.settings'
    _inherit = 'res.config.settings'

# GEOREFERENCIACIÓN
    module_l10n_ec_ote = fields.Boolean(
        'Usar la distribución geopolítica para la identificación geográfica de terceros.',
        help="""Instala el módulo l10n_ec_ote.""")
# SUPERCIAS
    module_l10n_ec_niif_base = fields.Boolean(
        'Instalar el plan de cuentas base para Pymes (SUPERCIAS).',
        help="""Instala la cuentas comunes a todas las Pymes en Ecuador""")
    module_l10n_ec_femd = fields.Boolean(
        'Utilizar el módulo de pagos para realizar el estado de flujos de efectivo con el método directo.',
        help="""Permite catalogar todos los cobros y pagos dentro de una categoría del estado de flujos de efectivo.""")
# SRI
    module_l10n_ec_sri = fields.Boolean(
        'Declaración de impuestos para sociedades y personas naturales obligadas a llevar contabilidad.',
        help="""Instala funcionalidades para el ATS y otras obligaciones con el SRI""")
    module_l10n_ec_sri_ce = fields.Boolean(
        'Gestionar compras y ventas con personas y sociedades del exterior.',
        help="""Crea posiciones fiscales e impuestos necesarios cuándo se realiza compras o ventas a empresas del exterior.""")
# RECURSOS HUMANOS
    module_l10n_ec_hr_payroll = fields.Boolean(
        'Gestionar la nómina de su empresa.',
        help="""Permite gestionar los pagos a la seguridad social, retenciones, décimos, entre otros.""")
    module_l10n_ec_hr_payroll_payment = fields.Boolean(
        'Pagos de nómina a través del rol de pagos.',
        help="""Permite realizar los pagos de los roles de pago a través del módulo de payroll.""")
# MEDIOS DE PAGO
    module_l10n_ec_payment = fields.Boolean(
        'Gestionar pagos para Ecuador: impresión de cheques, información de depósitos y transferencias.',
        help="""Instala el módulo l10n_ec_payment.""")
# UTILITARIOS
    # Usabilidad
    module_usability_use_popup = fields.Boolean(
        'Usar popup para ingresar información.',
        help="""Modifica el comportamiento de Odoo para ingresar datos en popup en lugar de editable tree, recomendable para vistas adaptativas y manejo del sistema en dispositivos móviles.""")
    # Gestión interna
    module_internal_docs = fields.Boolean(
        'Gestionar los documentos internos de la empresa.',
        help="""Permite llevar un control de los documento internos no obligatorios de la empresa como cheques, comprobantes de ingreso o egreso, entre otros.""")
    module_partner_contact_tradename = fields.Boolean(
        'Registrar el nombre comercial de sus clientes y proveedores.',
        help="""Instala el módulo partner_contact_tradename.""")
    # Mejoras generales
    module_l10n_ec_account = fields.Boolean(
        'Instalar las mejoras generales en el módulo de contabilidad.',
        help="""Agrega varias mejoras de usabilidad y presentación de formas adaptados a la realidad ecuatoriana, no es indispensable, pero se recomienda su instalación.""")
    module_l10n_ec_analytic = fields.Boolean(
        'Instalar las mejoras generales en el módulo de contabilidad analítica.',
        help="""Agrega varias mejoras de usabilidad y presentación de formas adaptados a la realidad ecuatoriana para contabilidad analítica, no es indispensable, pero se recomienda su instalación.""")
    # Extras no comunes
    module_extra_niif_payroll_account_map = fields.Boolean(
        'Mapear cuentas contables del rol de pagos con el catálogo NIIF Pymes.',
        help="""En caso de usar un plan de cuentas distinto a NIIF Base, deberá realizar el mapeo de cuentas de manera manual.""")
    module_extra_niif_sri_map = fields.Boolean(
        'Mapear cuentas contables de impuestos con el catálogo NIIF Pymes.',
        help="""En caso de usar un plan de cuentas distinto a NIIF Base, deberá realizar el mapeo de cuentas de manera manual.""")
