# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.exceptions import UserError


class ResCompany(models.Model):
    _inherit = 'res.company'

    numestabruc = fields.Char(
        string='Establecimientos activos', size=3, required=True,
        help="""Ingrese el numero de establecimientos activos
                inscritos en el R.U.C.""")
    vat_ec = fields.Char('Identificacion fiscal',
                         related='partner_id.vat_ec')

    property_account_position_id = fields.Many2one('account.fiscal.position', 'Posición fiscal',
                         related='partner_id.property_account_position_id')

    @api.multi
    @api.constrains('numestabruc')
    def _check_numestabruc(self):
        if len(self.numestabruc) != 3:
            raise UserError(
                    _("""El número de establecimientos se define en una cadena de tres caracteres. ej. 001"""))
