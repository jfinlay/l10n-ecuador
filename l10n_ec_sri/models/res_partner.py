# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.exceptions import UserError

from stdnum.ec import ruc, ci
from stdnum.exceptions import *

ADDRESS_FIELDS = ('vat_ec', 'street', 'street2', 'zip', 'city', 'state_id', 'country_id')

class ResPartner(models.Model):
    _inherit = 'res.partner'

    def _display_address(self, cr, uid, address, without_company=False, context=None):

        '''
        The purpose of this function is to build and return an address formatted accordingly to the
        standards of the country where it belongs.

        :param address: browse record of the res.partner to format
        :returns: the address formatted in a display that fit its country habits (or the default ones
            if not country is specified)
        :rtype: string
        '''

        # get the information that will be injected into the display format
        # get the address format
        address_format = address.country_id.address_format or \
                         "%(vat_ec)s\n%(street)s\n%(street2)s\n%(city)s %(state_code)s %(zip)s\n%(country_name)s"
        args = {
            'vat_ec': address.vat_ec or '',
            'state_code': address.state_id.code or '',
            'state_name': address.state_id.name or '',
            'country_code': address.country_id.code or '',
            'country_name': address.country_id.name or '',
            'company_name': address.parent_name or '',
        }
        for field in self._address_fields(cr, uid, context=context):
            args[field] = getattr(address, field) or ''
        if without_company:
            args['company_name'] = ''
        elif address.parent_id:
            address_format = '%(company_name)s\n' + address_format
        return address_format % args

    def _address_fields(self, cr, uid, context=None):
        """ Returns the list of address fields that are synced from the parent
        when the `use_parent_address` flag is set. """
        return list(ADDRESS_FIELDS)

    def _default_country_id(self):
        country = self.env['res.country'].search([('code', '=ilike', 'EC')])
        return country

    country_id = fields.Many2one(default=_default_country_id, )

    do_check_vat_ec = fields.Boolean('¿Validar la identificación?', default=True, )
    vat_ec = fields.Char('Identificacion fiscal', size=13, required=True)
    formapago_id = fields.Many2one(
        'l10n_ec_sri.formapago', string='Forma de pago principal', )
    detercero_ids = fields.One2many(
        'l10n_ec_sri.detercero', inverse_name='partner_id',
        ondelete='set null', string="Autorizaciones", )
    parterel = fields.Boolean(
        string="¿Es parte relacionada?", copy=False, )

    _sql_constraints = [('unique_vat_ec', 'UNIQUE(vat_ec)',
                         '¡Ya existe un contacto con esta Identificación Fiscal!\n'
                         'Tip: En el menú "Contactos" puede encontrar todos sus contactos'
                         )]

    # TODO: Cambiar las formulas para usar compute e inverse en lugar de onchanges
    @api.onchange('vat_ec', 'country_id')
    def _onchange_vat_ec(self):
        if not self.country_id:
            country_code = 'EC'
        else:
            country_code = self.country_id.code
        self.vat = country_code.upper() + str(self.vat_ec)

    @api.onchange('vat')
    def _onchange_vat(self):
        if self.vat:
            vat = self.vat
            vat_country, vat_number = vat[:2].lower(), vat[2:].replace(' ', '')
            country = self.env['res.country'].search([('code', '=ilike', vat_country)])
            self.country_id = country
            self.vat_ec = vat_number

    @api.onchange('property_account_position_id')
    def _onchange_property_account_position(self):
        if self.property_account_position_id:
            fiscal = self.property_account_position_id
            receivable = fiscal.property_account_receivable_id
            payable = fiscal.property_account_payable_id

            if not self.property_account_payable_id:
                self.property_account_payable_id = payable
            if not self.property_account_receivable_id:
                self.property_account_receivable_id = receivable

    @api.multi
    @api.constrains('vat_ec', 'property_account_position_id')
    def sri_check_vat_ec(self):
        for r in self:
            if r.do_check_vat_ec:
                fiscal = r.property_account_position_id
                persona = fiscal.persona_id.code
                identificacion = fiscal.identificacion_id.code
                if r.vat_ec and fiscal:
                    try:
                        if identificacion == 'R':
                            # Verificación de tipo de contribuyente
                            if persona == '6' and r.vat_ec[2:3] < '6':
                                pass
                            elif persona == '9' and r.vat_ec[2:3] == '6' and fiscal.es_publica:
                                pass
                            elif persona == '9' and r.vat_ec[2:3] == '9' and not fiscal.es_publica:
                                pass
                            else:
                                raise UserError(_("El numero de R.U.C. o C.I. no concuerda con el tipo de "
                                                  "contribuyente, por favor verifique que el numero sea correcto "
                                                  "en la página www.sri.gob.ec"))
                            # Verificación del documento
                            ruc.validate(r.vat_ec)
                        elif identificacion == 'C':
                            ci.validate(r.vat_ec)
                    except InvalidChecksum:
                        raise UserError(_("El numero de R.U.C. o C.I. no concuerda con el proceso de validacion "
                                          "del S.R.I., por favor verifique que el numero sea correcto en la "
                                          "página www.sri.gob.ec"))
                    except InvalidComponent:
                        if identificacion == 'R':
                            raise UserError(_("El numero de R.U.C. contiene errores, por favor verifique que "
                                              "los dos primeros dígitos se encuentren entre 01 y 24, que el tercer"
                                              "tercero digito no sea mayor que 5 y que el número de establecimiento sea"
                                              "válido."))
                        elif identificacion == 'C':
                            raise UserError(_("El numero de C.I. contiene errores, por favor verifique que "
                                              "los dos primeros dígitos se encuentren entre 01 y 24, que el tercer"
                                              "tercero digito no sea mayor que 5."))
                    except InvalidLength:
                        if identificacion == 'R':
                            raise UserError(_("El numero de R.U.C. debe tener 13 digitos, por favor verifique la "
                                              "información ingresada."))
                        elif identificacion == 'C':
                            raise UserError(_("El numero de C.I. debe tener 10 digitos, por favor verifique la "
                                              "información ingresada."))
                    except InvalidFormat:
                        raise UserError(_("El numero de R.U.C. o C.I. tiene caracteres no válidos, por favor "
                                          "verfique que la información ingresada sea correcta."))

    @api.v7
    def check_vat_ec(self, vat):
        """
        Necesitamos esta función para sobreescribir el chequeo del vat del módulo base_vat
        y evitar problemas de doble revisión. La podrémos eliminar cuándo Odoo cambie a la nueva API
        el módulo base_vat y por enede, podamos sobre sobreescribir la función check_vat_ec
        """
        return True
