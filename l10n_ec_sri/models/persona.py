# -*- coding: utf-8 -*-
from openerp import models, fields


class Persona(models.Model):
    _name = 'l10n_ec_sri.persona'

    name = fields.Char('Tipo de persona')
    code = fields.Char('Codigo', size=1)
    idprov = fields.Char('Tipo de identificacion del Proveedor',
                         size=2)
