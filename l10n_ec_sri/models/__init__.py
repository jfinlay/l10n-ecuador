# -*- coding: utf-8 -*-
from . import tipopago
from . import formapago
from . import account_payment
from . import account_journal
from . import res_partner
from . import res_company
from . import autorizacion
from . import comprobante
from . import sustento
from . import identificacion
from . import persona
from . import account_fiscal_position
from . import account
from . import account_invoice
