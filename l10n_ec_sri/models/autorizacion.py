# -*- coding: utf-8 -*-
from openerp import models, fields, api, _
from openerp.exceptions import UserError
from openerp.osv import expression


class Autorizacion(models.Model):
    _name = 'l10n_ec_sri.autorizacion'
    _description = "Autorizaciones"

    name = fields.Char(string="Autorizacion",)
    autorizacion = fields.Char('Nro. de autorizacion', required=True, related="name", )
    establecimiento = fields.Char('Establecimiento', size=3, required=True)
    puntoemision = fields.Char('Punto de impresion', size=3, required=True)
    fechaemision = fields.Date('Fecha de emision', )
    fechavencimiento = fields.Date('Fecha de vencimiento', required=True, )
    secuencia_inicial = fields.Integer('Secuencia inicial', )
    secuencia_final = fields.Integer('Secuencia final', )
    secuencia_actual = fields.Integer('Secuencia actual', help="Ingrese el numero de documento que va a utilizar en la secuencia", )
    comprobante_id = fields.Many2one('l10n_ec_sri.comprobante', string="Comprobante", required=True, domain=[('requiere_autorizacion','=', True)],)
    c_invoice_ids = fields.One2many('account.invoice', inverse_name='c_autorizacion_id', ondelete='restrict', string="Facturas")
    r_invoice_ids = fields.One2many('account.invoice', inverse_name='r_autorizacion_id', ondelete='restrict', string="Retenciones")
    comprobantesanulados_ids = fields.One2many('l10n_ec_sri.comprobantesanulados', inverse_name='autorizacion_id',
                                               ondelete='restrict', string="Comprobantes anulados", )
    revisar = fields.Char(string="Comprobantes no registrados",
                          compute="_compute_c_ids", )

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            name = record.establecimiento + '-' + record.puntoemision + '-' + record.autorizacion
            res.append((record.id, name))
        return res

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('establecimiento', '=ilike', name + '%'),
                      '|', ('puntoemision', '=ilike', name + '%'),
                      ('autorizacion', operator, name)]
            if operator in expression.NEGATIVE_TERM_OPERATORS:
                domain = ['&'] + domain
        autorizaciones = self.search(domain + args, limit=limit)
        return autorizaciones.name_get()

    @api.multi
    @api.depends('secuencia_inicial', 'secuencia_actual', 'c_invoice_ids', 'comprobantesanulados_ids')
    def _compute_c_ids(self):
        for r in self:
            anulados = list()
            revisar = ''
            for a in r.comprobantesanulados_ids:
                for i in range(a.secuencialinicio, (a.secuencialfin + 1), 1):
                    anulados.append(i)
            for n in range(r.secuencia_inicial, (r.secuencia_actual + 1), 1):
                if any(inv.secuencial == n for inv in r.c_invoice_ids) or any(inv.secuencial == n for inv in r.r_invoice_ids):
                    continue
                else:
                    if n not in anulados:
                        revisar += str(n) + ', '
                        r.revisar = revisar

    @api.multi
    @api.constrains('autorizacion', 'secuencia_inicial', 'secuencia_final', 'secuencia_actual')
    def _check_autorizacion(self):
        for r in self:
            if r.autorizacion:
                if len(r.autorizacion) not in (10, 37):
                    raise UserError(_("El numero de autorización debe ser igual a 10"
                                      " para documentos pre-impresos y 37 para documentos"
                                      " electrónicos."))

            if r.secuencia_inicial < 0 or r.secuencia_final < 0 \
                    or r.secuencia_actual < 0:
                raise UserError(_("Las secuencias deben ser mayores que cero."))

            if r.secuencia_final < r.secuencia_inicial:
                raise UserError(_("Las secuencia final no puede ser menor que la inicial."))

            if r.secuencia_final < r.secuencia_actual:
                raise UserError(_("La secuencia actual no debe ser mayor que la final."))

    @api.onchange('autorizacion')
    def _onchange_autorizacion(self):
        self.name = self.autorizacion


class DeTercero(models.Model):
    _name = 'l10n_ec_sri.detercero'
    _description = "Autorizaciones de terceros"

    name = fields.Char(string="Autorizacion",)
    autorizacion = fields.Char('Nro. de autorizacion', related="name", required=True, )
    establecimiento = fields.Char('Establecimiento', size=3, required=True)
    puntoemision = fields.Char('Punto de impresion', size=3, required=True)
    fechaemision = fields.Date('Fecha de emision', )
    fechavencimiento = fields.Date('Fecha de vencimiento', required=True, )
    secuencia_inicial = fields.Integer('Secuencia inicial')
    secuencia_final = fields.Integer('Secuencia final',)
    secuencia_actual = fields.Integer('Secuencia actual', help="Ingrese el número de documento que va a utilizar.", )
    comprobante_id = fields.Many2one(
        'l10n_ec_sri.comprobante', string="Comprobante",
        required=True, domain=[('requiere_autorizacion','=', True)], )
    partner_id = fields.Many2one('res.partner', string='Cliente/Proveedor', required=True, )
    c_invoice_ids = fields.One2many('account.invoice', inverse_name='c_detercero_id',
                                    ondelete='set null', string="Facturas", )
    r_invoice_ids = fields.One2many('account.invoice', inverse_name='r_detercero_id',
                                    ondelete='set null', string="Retenciones", )
    comprobantesanulados_ids = fields.One2many('l10n_ec_sri.comprobantesanulados', inverse_name='autorizacion_id',
                                               ondelete='restrict', string="Comprobantes anulados", )

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            name = record.establecimiento + '-' + record.puntoemision + '-' + record.autorizacion
            res.append((record.id, name))
        return res

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('establecimiento', '=ilike', name + '%'),
                      '|', ('puntoemision', '=ilike', name + '%'),
                      ('autorizacion', operator, name)]
            if operator in expression.NEGATIVE_TERM_OPERATORS:
                domain = ['&'] + domain
        autorizaciones = self.search(domain + args, limit=limit)
        return autorizaciones.name_get()

    @api.onchange('autorizacion')
    def _onchange_autorizacion(self):
        self.name = self.autorizacion

    @api.multi
    @api.constrains('autorizacion')
    def _check_autorizacion(self):
        for r in self:
            if r.autorizacion:
                if len(r.autorizacion) not in (10, 37):
                    raise UserError(_("El numero de autorización debe ser igual a 10"
                                      " para documentos pre-impresos y 37 para documentos"
                                      " electrónicos."))
