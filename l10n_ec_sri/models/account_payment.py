# -*- coding: utf-8 -*-
from openerp import models, fields, api


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    formapago_id = fields.Many2one(
        'l10n_ec_sri.formapago', string='Forma de pago')

    @api.onchange('journal_id')
    def _onchange_journal_sri(self):
        if self.journal_id:
            if not self.formapago_id:
                self.formapago_id = self.journal_id.formapago_id
