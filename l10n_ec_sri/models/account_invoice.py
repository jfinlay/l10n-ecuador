# -*- coding: utf-8 -*-

from openerp import models, fields, api, _
from datetime import datetime
from openerp.exceptions import UserError

retention_code = [
    '303', '304', '304A', '304B',
    '304C', '304D', '304E', '307',
    '308', '309', '310', '311',
    '312', '312A', '314A', '314B',
    '314C', '314D', '319', '320',
    '322', '323', '323A', '323B1',
    '323E', '323E2', '323F', '323G',
    '323H', '323I', '323 M', '323 N',
    '323 O', '323 P', '323Q', '323R',
    '324A', '324B', '325', '325A',
    '326', '327', '328', '329',
    '330', '331', '332', '332A',
    '332B', '332C', '332D', '332E',
    '332F', '332G', '332H', '332I',
    '333', '334', '335', '336',
    '337', '338', '339', '340',
    '341', '342', '342A', '342B',
    '343A', '343B', '344', '344A', '346A'
]

TYPE2REFUND = {
    'out_invoice': 'out_refund',        # Customer Invoice
    'in_invoice': 'in_refund',          # Vendor Bill
    'out_refund': 'out_invoice',        # Customer Refund
    'in_refund': 'in_invoice',          # Vendor Refund
}


class AccountInvoice(models.Model):
    _inherit = ['account.invoice']

    def _default_comprobante_id(self):
        comprobante = ''
        for inv in self:
            if inv.type in ('in_invoice', 'out_invoice'):
                comprobante = self.env['l10n_ec_sri.comprobante'].search([('code', '=', '01')], limit=1)
            elif inv.type in ('in_refund', 'out_refund'):
                comprobante = self.env['l10n_ec_sri.comprobante'].search([('code', '=', '04')], limit=1)
        return comprobante

    @api.multi
    def _default_date_invoice(self):
        return fields.Date.from_string(datetime.now().strftime('%Y-%m-%d'))

    documento_modificado_id = fields.Many2one(
        'account.invoice', string='Documento Modificado', )
    comprobante_id = fields.Many2one(
        'l10n_ec_sri.comprobante', string='Comprobante', default=_default_comprobante_id, copy=False, )
    r_comprobante_id = fields.Many2one(
        'l10n_ec_sri.comprobante', string='Comprobante retención', domain="[('es_retencion','=', True)]", copy=False, )
    r_secuencial = fields.Char(string='Secuencial retención', copy=False, )
    secuencial = fields.Char(
        string='Secuencial', help="En caso de no tener secuencia, debe ingresar nueves, ejemplo: 999999.", copy=False, )
    c_autorizacion_id = fields.Many2one('l10n_ec_sri.autorizacion', string=u'Autorización propia', copy=False, )
    c_detercero_id = fields.Many2one('l10n_ec_sri.detercero', string=u'Autorización de tercero', copy=False, )
    r_autorizacion_id = fields.Many2one('l10n_ec_sri.autorizacion', string=u'Autorización retención', copy=False, )
    r_detercero_id = fields.Many2one('l10n_ec_sri.detercero', string=u'Autorización retención tercero', copy=False, )
    r_date = fields.Date(string="Fecha", copy=False, )
    comprobante_code = fields.Char(
        string='Código de comprobante', related='comprobante_id.code', )
    comprobante_aut = fields.Boolean(
        string='¿Requiere autorización', related='comprobante_id.requiere_autorizacion', )
    date_invoice = fields.Date(
        string='Invoice Date', readonly=True, states={'draft': [('readonly', False)]}, index=True,
        help="Keep empty to use the current date", copy=False, default=_default_date_invoice, )
    basenograiva = fields.Monetary(
        string="Subtotal no grava I.V.A.", compute="_compute_invoice_values", )
    baseimponible = fields.Monetary(
        string="Subtotal I.V.A. 0%", compute="_compute_invoice_values", )
    baseimpgrav = fields.Monetary(
        string="Subtotal gravado con I.V.A.", compute="_compute_invoice_values", )
    baseimpexe = fields.Monetary(
        string="Subtotal excento de I.V.A.", compute="_compute_invoice_values", )
    baseintermediario = fields.Monetary(
        string="Subtotal Intermediario", compute="_compute_invoice_values", )
    montoiva = fields.Monetary(
        string="Monto I.V.A", compute="_compute_invoice_values", )
    montoice = fields.Monetary(
        string="Monto I.V.A", compute="_compute_invoice_values", )
    total = fields.Monetary(
        string='TOTAL', compute="_compute_invoice_values", )
    subtotal = fields.Monetary(
        string='SUBTOTAL', compute="_compute_invoice_values", )
    valornodeclarado = fields.Monetary(
        string="VALOR NO DECLARADO", compute="_compute_invoice_values", )

    # METODOS REDEFINIDOS:

    @api.multi
    def get_taxes_values(self):
        tax_grouped = {}
        for line in self.invoice_line_ids:
            price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = line.invoice_line_tax_ids.sorted(key=lambda r: r.sequence).compute_all(
                price_unit, self.currency_id, line.quantity, line.product_id, self.partner_id)['taxes']
            for tax in taxes:
                val = {
                    'invoice_id': self.id,
                    'name': tax['name'],
                    'tax_id': tax['id'],
                    'amount': tax['amount'],
                    'manual': False,
                    'sequence': tax['sequence'],
                    'account_analytic_id': tax['analytic'] and line.account_analytic_id.id or False,
                    'account_id': self.type in ('out_invoice', 'in_invoice') and (
                        tax['account_id'] or line.account_id.id) or (tax['refund_account_id'] or line.account_id.id),
                    'tax_base': tax['tax_base'],
                }

                # If the taxes generate moves on the same financial account as the invoice line,
                # propagate the analytic account from the invoice line to the tax line.
                # This is necessary in situations were (part of) the taxes cannot be reclaimed,
                # to ensure the tax move is allocated to the proper analytic account.
                if not val.get('account_analytic_id') and line.account_analytic_id \
                        and val['account_id'] == line.account_id.id:
                    val['account_analytic_id'] = line.account_analytic_id.id

                key = tax['id']
                if key not in tax_grouped:
                    tax_grouped[key] = val
                else:
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['tax_base'] += val['tax_base']
        return tax_grouped

    @api.multi
    def compute_taxes(self):
        """Function used in other module to compute the taxes on a fresh invoice created (onchanges did not applied)"""
        account_invoice_tax = self.env['account.invoice.tax']
        ctx = dict(self._context)
        for invoice in self:
            # Delete non-manual tax lines
            self._cr.execute("DELETE FROM account_invoice_tax WHERE invoice_id=%s", (invoice.id,))
            self.invalidate_cache()

            # Generate one tax line per tax, however many invoice lines it's applied to
            tax_grouped = invoice.get_taxes_values()

            # Create new tax lines
            for tax in tax_grouped.values():
                account_invoice_tax.create(tax)

        # dummy write on self to trigger recomputations
        return self.with_context(ctx).write({'invoice_line_ids': []})

    @api.model
    def _prepare_refund(self, invoice, date_invoice=None, date=None, description=None, journal_id=None):
        """ Prepare the dict of values to create the new refund from the invoice.
            This method may be overridden to implement custom
            refund generation (making sure to call super() to establish
            a clean extension chain).

            :param record invoice: invoice to refund
            :param string date_invoice: refund creation date from the wizard
            :param integer date: force date from the wizard
            :param string description: description of the refund from the wizard
            :param integer journal_id: account.journal from the wizard
            :return: dict of value to create() the refund
        """
        values = {}
        for field in ['name', 'reference', 'comment', 'date_due', 'partner_id', 'company_id',
                      'account_id', 'currency_id', 'payment_term_id', 'user_id', 'fiscal_position_id']:
            if invoice._fields[field].type == 'many2one':
                values[field] = invoice[field].id
            else:
                values[field] = invoice[field] or False

        values['invoice_line_ids'] = self._refund_cleanup_lines(invoice.invoice_line_ids)

        tax_lines = filter(lambda l: l.manual, invoice.tax_line_ids)
        values['tax_line_ids'] = self._refund_cleanup_lines(tax_lines)

        if journal_id:
            journal = self.env['account.journal'].browse(journal_id)
        elif invoice['type'] == 'in_invoice':
            journal = self.env['account.journal'].search([('type', '=', 'purchase')], limit=1)
        else:
            journal = self.env['account.journal'].search([('type', '=', 'sale')], limit=1)
        values['journal_id'] = journal.id

        values['type'] = TYPE2REFUND[invoice['type']]
        values['date_invoice'] = date_invoice or fields.Date.context_today(invoice)
        values['state'] = 'draft'
        values['number'] = False
        values['origin'] = invoice.number
        values['documento_modificado_id'] = invoice.id

        if date:
            values['date'] = date
        if description:
            values['name'] = description
        return values

    @api.model
    def _refund_cleanup_lines(self, lines):
        """ Convert records to dict of values suitable for one2many line creation

            :param recordset lines: records to convert
            :return: list of command tuple for one2many line creation [(0, 0, dict of valueis), ...]
        """
        result = []
        for line in lines:
            values = {}
            for name, field in line._fields.iteritems():
                if name in (
                        'id', 'create_uid', 'create_date', 'write_uid',
                        'write_date', 'basenograiva', 'baseimponible',
                        'baseimpgrav', 'baseimpexe', 'baseintermediario',
                        'montoiva', 'montoice', 'total', 'subtotal'):
                    continue
                elif field.type == 'many2one':
                    values[name] = line[name].id
                elif field.type not in ['many2many', 'one2many']:
                    values[name] = line[name]
#                elif name == 'invoice_line_tax_ids':
#                    values[name] = [(6, 0, None)]
            result.append((0, 0, values))
        return result

    # FIN DE METODOS REDEFINIDOS

    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        super(AccountInvoice, self)._onchange_partner_id()
        if self.type in ('in_invoice','in_refund'):
            self.c_autorizacion_id = ''
            self.c_detercero_id = ''

    @api.onchange('comprobante_id')
    def _onchange_comprobante_id(self):
        if self.comprobante_id != self.c_autorizacion_id.comprobante_id:
            self.c_autorizacion_id = ''
        elif self.comprobante_id != self.c_detercero_id.comprobante_id:
            self.c_detercero_id = ''

    @api.onchange('secuencial', 'c_autorizacion_id', 'c_detercero_id', 'comprobante_id')
    def onchange_secuencial(self):
        # Comprobantes que requieren secuancia pero no autorizacion
        if self.comprobante_id.code in ('11', '12', '19', '20') and not self.secuencial:
            self.secuencial = ''
        elif self.comprobante_id and not self.comprobante_id.requiere_autorizacion and not self.secuencial:
            self.secuencial = '999999999'
        else:
            if self.c_autorizacion_id and not (self.secuencial or self.secuencial == '999999999'):
                aut_obj = self.env['l10n_ec_sri.autorizacion'].browse(self.c_autorizacion_id.id)
                self.secuencial = str(self.c_autorizacion_id.secuencia_actual + 1)
                aut_obj.write({'secuencia_actual': int(self.secuencial)})

    @api.onchange('r_secuencial', 'r_autorizacion_id', 'r_detercero_id')
    def _onchange_r_secuencial(self):
        if self.r_autorizacion_id and not self.r_secuencial:
            aut_obj = self.env['l10n_ec_sri.autorizacion'].browse(self.r_autorizacion_id.id)
            self.r_secuencial = str(self.r_autorizacion_id.secuencia_actual + 1)
            aut_obj.write({'secuencia_actual': int(self.r_secuencial)})

    @api.multi
    @api.depends('invoice_line_ids')
    def _compute_invoice_values(self):
        for inv in self:
            # Suma las bases imponibles de la factura, se realiza desde invoice_line_ids puesto que con los
            # impuestos cuyo valor es cero, Odoo no genera un registro en tax_line_ids.
            inv.basenograiva = sum(l.basenograiva for l in inv.invoice_line_ids)
            inv.baseimponible = sum(l.baseimponible for l in inv.invoice_line_ids)
            inv.baseimpgrav = sum(l.baseimpgrav for l in inv.invoice_line_ids)
            inv.baseimpexe = sum(l.baseimpexe for l in inv.invoice_line_ids)
            inv.baseintermediario = sum(l.baseintermediario for l in inv.invoice_line_ids)
            # Suma todos los impuestos de la factura se hace desde tax_line_ids para reflejar los valores exactos
            # que se generaron en los registros contables.
            inv.montoiva = sum(
                tax.amount for tax in inv.tax_line_ids if tax.tax_id.tax_group_id.name == 'ImpGrav')
            inv.montoice = sum(
                tax.amount for tax in inv.tax_line_ids if tax.tax_id.tax_group_id.name == 'Ice')

            inv.subtotal = (inv.basenograiva + inv.baseimponible + inv.baseimpgrav + inv.baseimpexe + inv.baseintermediario)

            # Refleja al usuario los valores sin base imponible, que no serán usados en la declaración de impuestos.
            inv.valornodeclarado = sum(l.valornodeclarado for l in inv.invoice_line_ids)

            # Calculo del total, es necesario puesto que amount_total descuenta los valores de las retenciones.
            inv.total = (inv.subtotal + inv.montoiva + inv.montoice + inv.valornodeclarado)

    @api.multi
    def button_payments(self):
        return {
            'name': _('Pagos'),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'account.payment',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', [x.id for x in self.payment_ids])],
        }


    @api.multi
    @api.constrains('secuencial','comprobante_code','r_date','date_invoice')
    def check_invoice_values(self):
        for inv in self:
            if inv.comprobante_code and inv.secuencial:
                if len(inv.secuencial) > 9:
                    raise UserWarning(_("El número de secuencial debe tener menos de 10 dígitos."))
            if inv.r_date and inv.date_invoice > inv.r_date:
                raise UserWarning(_("La fecha de la retención no puede ser menor que la de la factura."))

    @api.multi
    @api.constrains('secuencial')
    def check_number(self):
        for inv in self:
            if inv.secuencial and not inv.secuencial.isdigit():
                raise UserError('El secuencial de la factura debe contener solo números')
            if inv.r_secuencial and not inv.r_secuencial.isdigit():
                raise UserError('El secuencial de la retención debe contener solo números')

class AccountInvoiceLine(models.Model):
    _inherit = ['account.invoice.line']


    basenograiva = fields.Monetary(compute="_compute_line_values", )
    baseimponible = fields.Monetary(compute="_compute_line_values", )
    baseimpgrav = fields.Monetary(compute="_compute_line_values", )
    baseimpexe = fields.Monetary(compute="_compute_line_values", )
    baseintermediario = fields.Monetary(compute="_compute_line_values", )
    valornodeclarado = fields.Monetary(compute="_compute_line_values", )
    # Campos para ATS por sustento de impuesto
    montoIva = fields.Monetary(string="Monto iva", )
    montoRetIva = fields.Monetary(string="Monto ret IVA", )
    montoRetIr = fields.Monetary(string="Monto ret renta", )
    montoDirecto = fields.Monetary(string="Monto directo", )
    monto322 = fields.Monetary(string="Monto 322", )
    sustento = fields.Char('Sustento')

    @api.multi
    @api.depends('invoice_line_tax_ids')
    def _compute_line_values(self):
        for line in self:
            if not any(tax.tax_group_id.name in ('ImpGrav', 'NoGraIva', 'Imponible', 'ImpExe', 'Intermediario')
                       for tax in line.invoice_line_tax_ids):
                line.valornodeclarado = line.price_subtotal
            else:
                for tax in line.invoice_line_tax_ids:
                    if tax.tax_group_id.name == 'ImpGrav':
                        line.baseimpgrav = line.price_subtotal
                    elif tax.tax_group_id.name == 'NoGraIva':
                        line.basenograiva = line.price_subtotal
                    elif tax.tax_group_id.name == 'Imponible':
                        line.baseimponible = line.price_subtotal
                    elif tax.tax_group_id.name == 'ImpExe':
                        line.baseimpexe = line.price_subtotal
                    elif tax.tax_group_id.name == 'Intermediario':
                        line.baseintermediario = line.price_subtotal

    """ESTA FÓRMULA ES USADA PARA CALCULAR LOS IMPUESTOS POR CADA LÍNEA, CUANDO SE TERMINE DE HACER LA REESCRITURA DEL
    ATS PARA CALCULAR EN BASE A LAS LÍNEAS DE IMPUESTOS YA NO SERÁ NECESARIA, ELIMINAR CON PRECAUCIÓN."""
    @api.multi
    def compute_inline_taxes(self, line, tax_ids):
        tax_amount = 0.0
        for tax in tax_ids:
            # CALCULOS ESPECIALES.
            # Permite crear retenciones de emisores de tarjetas de credito
            if any(tag.name == 'Directo' for tag in tax.tag_ids):
                tax_amount = abs(line.quantity)
                line.montoDirecto = tax_amount
            # Permite calcular el 1% del 10% para la retencion de primas de seguros.
            elif tax.sri_code == '322':
                tax_amount = abs(round((line.price_subtotal * 0.10), 2) * abs(tax.amount / 100))
                line.monto322 = tax_amount
            # Calcula un porcentaje de retención sobre el IVA, se realiza en codigo para realizar doble redondeo.
            elif tax.tax_group_id.name in (
                    'RetIva', 'RetBien10', 'RetBienes', 'RetServ100', 'RetServ20', 'RetServicios'):

                # Calcula el porcentaje de IVA en la línea de factura.
                iva = 0.0
                for t in line.invoice_line_tax_ids:
                    if t.tax_group_id.name == 'ImpGrav':
                        iva = t.amount

                # Calcula el valor de la retención si el IVA es mayor que cero.
                if iva > 0:
                    tax_amount = abs(round(line.price_subtotal * (iva / 100), 2) * abs(tax.amount / 100))
                    line.montoRetIva = tax_amount

            elif tax.tax_group_id.name == 'RetIr':
                tax_amount = abs(round(line.price_subtotal, 2) * abs(tax.amount / 100))
                line.montoRetIr = tax_amount
            # Aplica los impuestos normales de odoo
            else:
                # TODO Calcular impuestos para fixed type.
                # if tax.amount_type == 'fixed':
                # return math.copysign(self.amount, base_amount) * quantity
                if (tax.amount_type == 'percent' and not tax.price_include) or (
                                tax.amount_type == 'division' and tax.price_include):
                    tax_amount = abs(line.price_subtotal * tax.amount / 100)
                elif tax.amount_type == 'percent' and tax.price_include:
                    tax_amount = abs(line.price_subtotal - (line.price_subtotal / (1 + tax.amount / 100)))
                elif tax.amount_type == 'division' and not tax.price_include:
                    tax_amount = abs(line.price_subtotal / (1 - tax.amount / 100) - line.price_subtotal)
                if tax_amount:
                    line.montoIva = tax_amount
                    line.sustento = tax.sustento_id.code if tax.sustento_id else False
        return tax_amount


class AccountInvoiceTax(models.Model):
    _inherit = "account.invoice.tax"

    tax_base = fields.Monetary(string="Base Imponible", )
