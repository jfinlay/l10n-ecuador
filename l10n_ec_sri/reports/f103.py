# -*- coding: utf-8 -*-
from openerp import models, fields, api


class F103(models.Model):
    _name = "l10n_ec_sri.f103"
    _description = "103"


    name = fields.Char('Formulario Nro.', )

    state = fields.Selection([('draft', 'Borrador'),
                              ('posted', 'Presentado'),
                              ('cancelled', 'Sustituido'),
                              ],
                             string='Estado',
                             default='draft', )

    # Definir el mes por defecto del reporte
    def _default_month(self):
        month = fields.datetime.now().strftime("%m")
        if month == '01':
            default = '12'
        else:
            default = str(int(month) - 1).zfill(2)
        return default

    c101 = fields.Selection([('01', 'Enero'),
                             ('02', 'Febrero'),
                             ('03', 'Marzo'),
                             ('04', 'Abril'),
                             ('05', 'Mayo'),
                             ('06', 'Junio'),
                             ('07', 'Julio'),
                             ('08', 'Agosto'),
                             ('09', 'Septiembre'),
                             ('10', 'Octubre'),
                             ('11', 'Noviembre'),
                             ('12', 'Diciembre')
                             ],
                            string='Mes',
                            default=_default_month,
                            required=True)

    # Definir el ano por defecto del reporte
    def _default_year(self):
        month = fields.datetime.now().strftime("%m")
        year = fields.datetime.now().strftime("%Y")
        if month == '01':
            default = str(int(year) - 1)
        else:
            default = str(year)
        return default

    c102 = fields.Char(
        'Año', default=_default_year, required=True, )

    c104 = fields.Char('Formulario que sustituye', )

    c302 = fields.Float('302', readonly=False, )
    c352 = fields.Float('352', readonly=False, )
    c303 = fields.Float('303', readonly=False, )
    c353 = fields.Float('353', readonly=False, )
    c304 = fields.Float('304', readonly=False, )
    c354 = fields.Float('354', readonly=False, )
    c307 = fields.Float('307', readonly=False, )
    c357 = fields.Float('357', readonly=False, )
    c308 = fields.Float('308', readonly=False, )
    c358 = fields.Float('358', readonly=False, )
    c309 = fields.Float('309', readonly=False, )
    c359 = fields.Float('359', readonly=False, )
    c310 = fields.Float('310', readonly=False, )
    c360 = fields.Float('360', readonly=False, )
    c311 = fields.Float('311', readonly=False, )
    c361 = fields.Float('361', readonly=False, )
    c312 = fields.Float('312', readonly=False, )
    c362 = fields.Float('362', readonly=False, )
    c314 = fields.Float('314', readonly=False, )
    c364 = fields.Float('364', readonly=False, )
    c319 = fields.Float('319', readonly=False, )
    c369 = fields.Float('369', readonly=False, )
    c320 = fields.Float('320', readonly=False, )
    c370 = fields.Float('370', readonly=False, )
    c322 = fields.Float('322', readonly=False, )
    c372 = fields.Float('372', readonly=False, )
    c323 = fields.Float('323', readonly=False, )
    c373 = fields.Float('373', readonly=False, )
    c324 = fields.Float('324', readonly=False, )
    c374 = fields.Float('374', readonly=False, )
    c325 = fields.Float('325', readonly=False, )
    c375 = fields.Float('375', readonly=False, )
    c326 = fields.Float('326', readonly=False, )
    c376 = fields.Float('376', readonly=False, )
    c327 = fields.Float('327', readonly=False, )
    c377 = fields.Float('377', readonly=False, )
    c328 = fields.Float('328', readonly=False, )
    c378 = fields.Float('378', readonly=False, )
    c329 = fields.Float('329', readonly=False, )
    c379 = fields.Float('379', readonly=False, )
    c330 = fields.Float('330', readonly=False, )
    c380 = fields.Float('380', readonly=False, )
    c331 = fields.Float('331', readonly=False, )
    c332 = fields.Float('332', readonly=False, )
    c333 = fields.Float('333', readonly=False, )
    c383 = fields.Float('383', readonly=False, )
    c334 = fields.Float('334', readonly=False, )
    c384 = fields.Float('384', readonly=False, )
    c335 = fields.Float('335', readonly=False, )
    c385 = fields.Float('385', readonly=False, )
    c336 = fields.Float('336', readonly=False, )
    c386 = fields.Float('386', readonly=False, )
    c337 = fields.Float('337', readonly=False, )
    c387 = fields.Float('387', readonly=False, )
    c338 = fields.Float('338', readonly=False, )
    c388 = fields.Float('388', readonly=False, )
    c339 = fields.Float('339', readonly=False, )
    c389 = fields.Float('389', readonly=False, )
    c340 = fields.Float('340', readonly=False, )
    c390 = fields.Float('390', readonly=False, )
    c341 = fields.Float('341', readonly=False, )
    c391 = fields.Float('391', readonly=False, )
    c342 = fields.Float('342', readonly=False, )
    c392 = fields.Float('392', readonly=False, )
    c343 = fields.Float('343', readonly=False, )
    c393 = fields.Float('393', readonly=False, )
    c344 = fields.Float('344', readonly=False, )
    c394 = fields.Float('394', readonly=False, )
    c345 = fields.Float('345', readonly=False, )
    c395 = fields.Float('395', readonly=False, )
    c346 = fields.Float('346', readonly=False, )
    c396 = fields.Float('396', readonly=False, )

    c349 = fields.Float('349', readonly=False, )
    c399 = fields.Float('399', readonly=False, )

    c402 = fields.Float('402', readonly=False, )
    c452 = fields.Float('452', readonly=False, )
    c403 = fields.Float('403', readonly=False, )
    c453 = fields.Float('453', readonly=False, )
    c404 = fields.Float('404', readonly=False, )
    c454 = fields.Float('454', readonly=False, )
    c405 = fields.Float('405', readonly=False, )
    c406 = fields.Float('406', readonly=False, )
    c456 = fields.Float('456', readonly=False, )
    c407 = fields.Float('407', readonly=False, )
    c457 = fields.Float('457', readonly=False, )
    c408 = fields.Float('408', readonly=False, )
    c458 = fields.Float('458', readonly=False, )
    c409 = fields.Float('409', readonly=False, )
    c459 = fields.Float('459', readonly=False, )
    c410 = fields.Float('410', readonly=False, )
    c460 = fields.Float('460', readonly=False, )
    c411 = fields.Float('411', readonly=False, )
    c461 = fields.Float('461', readonly=False, )
    c412 = fields.Float('412', readonly=False, )
    c413 = fields.Float('413', readonly=False, )
    c463 = fields.Float('463', readonly=False, )
    c414 = fields.Float('414', readonly=False, )
    c464 = fields.Float('464', readonly=False, )
    c415 = fields.Float('415', readonly=False, )
    c465 = fields.Float('465', readonly=False, )
    c416 = fields.Float('416', readonly=False, )
    c417 = fields.Float('417', readonly=False, )
    c467 = fields.Float('467', readonly=False, )
    c418 = fields.Float('418', readonly=False, )
    c468 = fields.Float('468', readonly=False, )
    c419 = fields.Float('419', readonly=False, )
    c469 = fields.Float('469', readonly=False, )
    c420 = fields.Float('420', readonly=False, )
    c470 = fields.Float('470', readonly=False, )
    c421 = fields.Float('421', readonly=False, )
    c471 = fields.Float('471', readonly=False, )
    c422 = fields.Float('422', readonly=False, )
    c472 = fields.Float('472', readonly=False, )
    c423 = fields.Float('423', readonly=False, )
    c424 = fields.Float('424', readonly=False, )
    c474 = fields.Float('474', readonly=False, )
    c425 = fields.Float('425', readonly=False, )
    c475 = fields.Float('475', readonly=False, )
    c426 = fields.Float('426', readonly=False, )
    c476 = fields.Float('476', readonly=False, )
    c427 = fields.Float('427', readonly=False, )
    c477 = fields.Float('477', readonly=False, )
    c428 = fields.Float('428', readonly=False, )
    c478 = fields.Float('478', readonly=False, )
    c429 = fields.Float('429', readonly=False, )
    c479 = fields.Float('479', readonly=False, )
    c430 = fields.Float('430', readonly=False, )
    c480 = fields.Float('480', readonly=False, )
    c431 = fields.Float('431', readonly=False, )
    c481 = fields.Float('481', readonly=False, )
    c432 = fields.Float('432', readonly=False, )
    c482 = fields.Float('482', readonly=False, )
    c433 = fields.Float('433', readonly=False, )

    c497 = fields.Float('497', readonly=False, )
    c498 = fields.Float('498', readonly=False, )

    @api.multi
    @api.onchange('c101', 'c102')
    def _onchange_103_values(self):
        for r in self:
            # TODO Cambiar el codigo para formatear todos los campos excepto c101 y c102 en un solo paso para evitar errores.
            # Reinicia los valores a 0 al cambiar las fechas.
            r.c302 = 0.0
            r.c352 = 0.0
            r.c303 = 0.0
            r.c353 = 0.0
            r.c304 = 0.0
            r.c354 = 0.0
            r.c307 = 0.0
            r.c357 = 0.0
            r.c308 = 0.0
            r.c358 = 0.0
            r.c309 = 0.0
            r.c359 = 0.0
            r.c310 = 0.0
            r.c360 = 0.0
            r.c311 = 0.0
            r.c361 = 0.0
            r.c312 = 0.0
            r.c362 = 0.0
            r.c314 = 0.0
            r.c364 = 0.0
            r.c319 = 0.0
            r.c369 = 0.0
            r.c320 = 0.0
            r.c370 = 0.0
            r.c322 = 0.0
            r.c372 = 0.0
            r.c323 = 0.0
            r.c373 = 0.0
            r.c324 = 0.0
            r.c374 = 0.0
            r.c325 = 0.0
            r.c375 = 0.0
            r.c326 = 0.0
            r.c376 = 0.0
            r.c327 = 0.0
            r.c377 = 0.0
            r.c328 = 0.0
            r.c378 = 0.0
            r.c329 = 0.0
            r.c379 = 0.0
            r.c330 = 0.0
            r.c380 = 0.0
            r.c331 = 0.0
            r.c332 = 0.0
            r.c333 = 0.0
            r.c383 = 0.0
            r.c334 = 0.0
            r.c384 = 0.0
            r.c335 = 0.0
            r.c385 = 0.0
            r.c336 = 0.0
            r.c386 = 0.0
            r.c337 = 0.0
            r.c387 = 0.0
            r.c338 = 0.0
            r.c388 = 0.0
            r.c339 = 0.0
            r.c389 = 0.0
            r.c340 = 0.0
            r.c390 = 0.0
            r.c341 = 0.0
            r.c391 = 0.0
            r.c342 = 0.0
            r.c392 = 0.0
            r.c343 = 0.0
            r.c393 = 0.0
            r.c344 = 0.0
            r.c394 = 0.0
            r.c345 = 0.0
            r.c395 = 0.0
            r.c346 = 0.0
            r.c396 = 0.0
            r.c402 = 0.0
            r.c452 = 0.0
            r.c403 = 0.0
            r.c453 = 0.0
            r.c404 = 0.0
            r.c454 = 0.0
            r.c405 = 0.0
            r.c406 = 0.0
            r.c456 = 0.0
            r.c407 = 0.0
            r.c457 = 0.0
            r.c408 = 0.0
            r.c458 = 0.0
            r.c409 = 0.0
            r.c459 = 0.0
            r.c410 = 0.0
            r.c460 = 0.0
            r.c411 = 0.0
            r.c461 = 0.0
            r.c412 = 0.0
            r.c413 = 0.0
            r.c463 = 0.0
            r.c414 = 0.0
            r.c464 = 0.0
            r.c415 = 0.0
            r.c465 = 0.0
            r.c416 = 0.0
            r.c417 = 0.0
            r.c467 = 0.0
            r.c418 = 0.0
            r.c468 = 0.0
            r.c419 = 0.0
            r.c469 = 0.0
            r.c420 = 0.0
            r.c470 = 0.0
            r.c421 = 0.0
            r.c471 = 0.0
            r.c422 = 0.0
            r.c472 = 0.0
            r.c423 = 0.0
            r.c424 = 0.0
            r.c474 = 0.0
            r.c425 = 0.0
            r.c475 = 0.0
            r.c426 = 0.0
            r.c476 = 0.0
            r.c427 = 0.0
            r.c477 = 0.0
            r.c428 = 0.0
            r.c478 = 0.0
            r.c429 = 0.0
            r.c479 = 0.0
            r.c430 = 0.0
            r.c480 = 0.0
            r.c431 = 0.0
            r.c481 = 0.0
            r.c432 = 0.0
            r.c482 = 0.0
            r.c433 = 0.0

            # Obtiene las lineas de compras
            purchase_lines = self.env['account.invoice.line'].search([]).filtered(
                lambda l: str(l.invoice_id.date_invoice)[0:4] == r.c102 and
                          str(l.invoice_id.date_invoice)[5:7] == r.c101 and
                          l.invoice_id.type == 'in_invoice' and
                          l.invoice_id.state in ('open', 'paid'))

            for line in purchase_lines:
                # Evita la declaración de facturas sin comprobante válido.
                if not line.invoice_id.comprobante_id.code:
                    continue

                else:

                    # Señala la variable para los impuesto de línea de factura
                    tax_ids = line.invoice_line_tax_ids

                    # Verifica si existe al menos una retención del IR
                    len_retir = sum(1 for tax in tax_ids if tax.tax_group_id.name == 'RetIr')

                    # Si no existe retención o si no hay un comprobante de retención ingresado
                    # envía la línea de factura al casillero 332.
                    if len_retir == 0 or line.invoice_id.r_secuencial == 0:
                        r.c332 += line.price_subtotal

                    # Si existe comprobante de retención y alguna retención, calcula el impuesto
                    # y lo asigna al casillero correspondiente basándose en los tags.
                    else:
                        for tax in tax_ids:
                            if tax.tax_group_id.name == 'RetIr':
                                tax_amount = line.compute_inline_taxes(line, tax)
                                for tag in tax.tag_ids:
                                    if tag.name == '103_302':
                                        r.c302 += line.price_subtotal
                                        r.c352 += abs(tax_amount)
                                    elif tag.name == '103_303':
                                        r.c303 += line.price_subtotal
                                        r.c353 += abs(tax_amount)
                                    elif tag.name == '103_304':
                                        r.c304 += line.price_subtotal
                                        r.c354 += abs(tax_amount)
                                    elif tag.name == '103_307':
                                        r.c307 += line.price_subtotal
                                        r.c357 += abs(tax_amount)
                                    elif tag.name == '103_308':
                                        r.c308 += line.price_subtotal
                                        r.c358 += abs(tax_amount)
                                    elif tag.name == '103_309':
                                        r.c309 += line.price_subtotal
                                        r.c359 += abs(tax_amount)
                                    elif tag.name == '103_310':
                                        r.c310 += line.price_subtotal
                                        r.c360 += abs(tax_amount)
                                    elif tag.name == '103_311':
                                        r.c311 += line.price_subtotal
                                        r.c361 += abs(tax_amount)
                                    elif tag.name == '103_312':
                                        r.c312 += line.price_subtotal
                                        r.c362 += abs(tax_amount)
                                    elif tag.name == '103_314':
                                        r.c314 += line.price_subtotal
                                        r.c364 += abs(tax_amount)
                                    elif tag.name == '103_319':
                                        r.c319 += line.price_subtotal
                                        r.c369 += abs(tax_amount)
                                    elif tag.name == '103_320':
                                        r.c320 += line.price_subtotal
                                        r.c370 += abs(tax_amount)
                                    elif tag.name == '103_322':
                                        r.c322 += (line.price_subtotal * 0.10)
                                        r.c332 += (line.price_subtotal * 0.90)
                                        r.c372 += abs(tax_amount)
                                    elif tag.name == '103_323':
                                        r.c323 += line.price_subtotal
                                        r.c373 += abs(tax_amount)
                                    elif tag.name == '103_324':
                                        r.c324 += line.price_subtotal
                                        r.c374 += abs(tax_amount)
                                    elif tag.name == '103_325':
                                        r.c325 += line.price_subtotal
                                        r.c375 += abs(tax_amount)
                                    elif tag.name == '103_326':
                                        r.c326 += line.price_subtotal
                                        r.c376 += abs(tax_amount)
                                    elif tag.name == '103_327':
                                        r.c327 += line.price_subtotal
                                        r.c377 += abs(tax_amount)
                                    elif tag.name == '103_328':
                                        r.c328 += line.price_subtotal
                                        r.c378 += abs(tax_amount)
                                    elif tag.name == '103_329':
                                        r.c329 += line.price_subtotal
                                        r.c379 += abs(tax_amount)
                                    elif tag.name == '103_330':
                                        r.c330 += line.price_subtotal
                                        r.c380 += abs(tax_amount)
                                    elif tag.name == '103_331':
                                        r.c331 += line.price_subtotal
                                    elif tag.name == '103_332':
                                        r.c332 += line.price_subtotal
                                    elif tag.name == '103_333':
                                        r.c333 += line.price_subtotal
                                        r.c383 += abs(tax_amount)
                                    elif tag.name == '103_334':
                                        r.c334 += line.price_subtotal
                                        r.c384 += abs(tax_amount)
                                    elif tag.name == '103_335':
                                        r.c335 += line.price_subtotal
                                        r.c385 += abs(tax_amount)
                                    elif tag.name == '103_336':
                                        r.c336 += line.price_subtotal
                                        r.c386 += abs(tax_amount)
                                    elif tag.name == '103_337':
                                        r.c337 += line.price_subtotal
                                        r.c387 += abs(tax_amount)
                                    elif tag.name == '103_338':
                                        r.c338 += line.price_subtotal
                                        r.c388 += abs(tax_amount)
                                    elif tag.name == '103_339':
                                        r.c339 += line.price_subtotal
                                        r.c389 += abs(tax_amount)
                                    elif tag.name == '103_340':
                                        r.c340 += line.price_subtotal
                                        r.c390 += abs(tax_amount)
                                    elif tag.name == '103_341':
                                        r.c341 += line.price_subtotal
                                        r.c391 += abs(tax_amount)
                                    elif tag.name == '103_342':
                                        r.c342 += line.price_subtotal
                                        r.c392 += abs(tax_amount)
                                    elif tag.name == '103_343':
                                        r.c343 += line.price_subtotal
                                        r.c393 += abs(tax_amount)
                                    elif tag.name == '103_344':
                                        r.c344 += line.price_subtotal
                                        r.c394 += abs(tax_amount)
                                    elif tag.name == '103_345':
                                        r.c345 += line.price_subtotal
                                        r.c395 += abs(tax_amount)
                                    elif tag.name == '103_346':
                                        r.c346 += line.price_subtotal
                                        r.c396 += abs(tax_amount)
                                    elif tag.name == '103_402':
                                        r.c402 += line.price_subtotal
                                        r.c452 += abs(tax_amount)
                                    elif tag.name == '103_403':
                                        r.c403 += line.price_subtotal
                                        r.c453 += abs(tax_amount)
                                    elif tag.name == '103_404':
                                        r.c404 += line.price_subtotal
                                        r.c454 += abs(tax_amount)
                                    elif tag.name == '103_405':
                                        r.c405 += line.price_subtotal
                                    elif tag.name == '103_406':
                                        r.c406 += line.price_subtotal
                                        r.c456 += abs(tax_amount)
                                    elif tag.name == '103_407':
                                        r.c407 += line.price_subtotal
                                        r.c457 += abs(tax_amount)
                                    elif tag.name == '103_408':
                                        r.c408 += line.price_subtotal
                                        r.c458 += abs(tax_amount)
                                    elif tag.name == '103_409':
                                        r.c409 += line.price_subtotal
                                        r.c459 += abs(tax_amount)
                                    elif tag.name == '103_410':
                                        r.c410 += line.price_subtotal
                                        r.c460 += abs(tax_amount)
                                    elif tag.name == '103_411':
                                        r.c411 += line.price_subtotal
                                        r.c461 += abs(tax_amount)
                                    elif tag.name == '103_412':
                                        r.c412 += line.price_subtotal
                                    elif tag.name == '103_413':
                                        r.c413 += line.price_subtotal
                                        r.c463 += abs(tax_amount)
                                    elif tag.name == '103_414':
                                        r.c414 += line.price_subtotal
                                        r.c464 += abs(tax_amount)
                                    elif tag.name == '103_415':
                                        r.c415 += line.price_subtotal
                                        r.c465 += abs(tax_amount)
                                    elif tag.name == '103_416':
                                        r.c416 += line.price_subtotal
                                    elif tag.name == '103_417':
                                        r.c417 += line.price_subtotal
                                        r.c467 += abs(tax_amount)
                                    elif tag.name == '103_418':
                                        r.c418 += line.price_subtotal
                                        r.c468 += abs(tax_amount)
                                    elif tag.name == '103_419':
                                        r.c419 += line.price_subtotal
                                        r.c469 += abs(tax_amount)
                                    elif tag.name == '103_420':
                                        r.c420 += line.price_subtotal
                                        r.c470 += abs(tax_amount)
                                    elif tag.name == '103_421':
                                        r.c421 += line.price_subtotal
                                        r.c471 += abs(tax_amount)
                                    elif tag.name == '103_422':
                                        r.c422 += line.price_subtotal
                                        r.c472 += abs(tax_amount)
                                    elif tag.name == '103_423':
                                        r.c423 += line.price_subtotal
                                    elif tag.name == '103_424':
                                        r.c424 += line.price_subtotal
                                        r.c474 += abs(tax_amount)
                                    elif tag.name == '103_425':
                                        r.c425 += line.price_subtotal
                                        r.c475 += abs(tax_amount)
                                    elif tag.name == '103_426':
                                        r.c426 += line.price_subtotal
                                        r.c476 += abs(tax_amount)
                                    elif tag.name == '103_427':
                                        r.c427 += line.price_subtotal
                                        r.c477 += abs(tax_amount)
                                    elif tag.name == '103_428':
                                        r.c428 += line.price_subtotal
                                        r.c478 += abs(tax_amount)
                                    elif tag.name == '103_429':
                                        r.c429 += line.price_subtotal
                                        r.c479 += abs(tax_amount)
                                    elif tag.name == '103_430':
                                        r.c430 += line.price_subtotal
                                        r.c480 += abs(tax_amount)
                                    elif tag.name == '103_431':
                                        r.c431 += line.price_subtotal
                                        r.c481 += abs(tax_amount)
                                    elif tag.name == '103_432':
                                        r.c432 += line.price_subtotal
                                        r.c482 += abs(tax_amount)
                                    elif tag.name == '103_433':
                                        r.c433 += line.price_subtotal

            r.c349 = (r.c302 + r.c303 + r.c304 + r.c307 + r.c308 + r.c309 +
                      r.c310 + r.c311 + r.c312 + r.c314 + r.c319 + r.c320 +
                      r.c322 + r.c323 + r.c324 + r.c325 + r.c326 + r.c327 +
                      r.c328 + r.c329 + r.c330 + r.c331 + r.c332 + r.c333 +
                      r.c334 + r.c335 + r.c336 + r.c337 + r.c338 + r.c339 +
                      r.c340 + r.c341 + r.c342 + r.c343 + r.c344 + r.c345 +
                      r.c346)

            r.c399 = (r.c352 + r.c353 + r.c354 + r.c357 + r.c358 + r.c359 +
                      r.c360 + r.c361 + r.c362 + r.c364 + r.c369 + r.c370 +
                      r.c372 + r.c373 + r.c374 + r.c375 + r.c376 + r.c377 +
                      r.c378 + r.c379 + r.c380 + r.c383 + r.c384 + r.c385 +
                      r.c386 + r.c387 + r.c388 + r.c389 + r.c390 + r.c391 +
                      r.c392 + r.c393 + r.c394 + r.c395 + r.c396)

            r.c497 = (r.c402 + r.c403 + r.c404 + r.c405 + r.c406 + r.c407 +
                      r.c408 + r.c409 + r.c410 + r.c411 + r.c412 + r.c413 +
                      r.c414 + r.c415 + r.c416 + r.c417 + r.c418 + r.c419 +
                      r.c420 + r.c421 + r.c422 + r.c423 + r.c424 + r.c425 +
                      r.c426 + r.c427 + r.c428 + r.c429 + r.c430 + r.c431 +
                      r.c432 + r.c433)

            r.c498 = (r.c452 + r.c453 + r.c454 + r.c456 + r.c457 + r.c458 +
                      r.c459 + r.c460 + r.c461 + r.c463 + r.c464 + r.c465 +
                      r.c467 + r.c468 + r.c469 + r.c470 + r.c471 + r.c472 +
                      r.c474 + r.c475 + r.c476 + r.c477 + r.c478 + r.c479 +
                      r.c480 + r.c481 + r.c482)

    @api.multi
    def print_f103(self):
        return self.env['report'].get_action(self, 'l10n_ec_sri.f103_report')
