# -*- coding: utf-8 -*-
from openerp import models, fields, api


class F104(models.Model):
    _name = "l10n_ec_sri.f104"
    _description = "104"

    name = fields.Char('Formulario Nro.', )

    state = fields.Selection([('draft', 'Borrador'),
                              ('posted', 'Presentado'),
                              ('cancelled', 'Sustituido'),
                              ],
                             string='Estado',
                             default='draft', )

    # Definir el mes por defecto del reporte
    def _default_month(self):
        month = fields.datetime.now().strftime("%m")
        if month == '01':
            default = '12'
        else:
            default = str(int(month) - 1).zfill(2)
        return default

    c101 = fields.Selection([('01', 'Enero'),
                             ('02', 'Febrero'),
                             ('03', 'Marzo'),
                             ('04', 'Abril'),
                             ('05', 'Mayo'),
                             ('06', 'Junio'),
                             ('07', 'Julio'),
                             ('08', 'Agosto'),
                             ('09', 'Septiembre'),
                             ('10', 'Octubre'),
                             ('11', 'Noviembre'),
                             ('12', 'Diciembre')
                             ],
                            string='Mes',
                            default=_default_month,
                            required=True)

    # Definir el ano por defecto del reporte
    def _default_year(self):
        month = fields.datetime.now().strftime("%m")
        year = fields.datetime.now().strftime("%Y")
        if month == '01':
            default = str(int(year) - 1)
        else:
            default = str(year)
        return default

    c102 = fields.Char(
        'Año', default=_default_year, required=True, )

    c104 = fields.Char('Formulario que sustituye', )

    c401 = fields.Float('401', readonly=False, )
    c411 = fields.Float('411', readonly=False, )
    c421 = fields.Float('421', readonly=False, )

    c402 = fields.Float('402', readonly=False, )
    c412 = fields.Float('412', readonly=False, )
    c422 = fields.Float('422', readonly=False, )

    c403 = fields.Float('403', readonly=False, )
    c413 = fields.Float('413', readonly=False, )

    c404 = fields.Float('404', readonly=False, )
    c414 = fields.Float('414', readonly=False, )

    c405 = fields.Float('405', readonly=False, )
    c415 = fields.Float('415', readonly=False, )

    c406 = fields.Float('406', readonly=False, )
    c416 = fields.Float('416', readonly=False, )

    c407 = fields.Float('407', readonly=False, )
    c417 = fields.Float('417', readonly=False, )

    c408 = fields.Float('408', readonly=False, )
    c418 = fields.Float('418', readonly=False, )

    c409 = fields.Float('409', readonly=False, )
    c419 = fields.Float('419', readonly=False, )
    c429 = fields.Float('429', readonly=False, )

    c431 = fields.Float('431', readonly=False, )
    c441 = fields.Float('441', readonly=False, )

    c442 = fields.Float('442', readonly=False, )

    c443 = fields.Float('443', readonly=False, )
    c453 = fields.Float('453', readonly=False, )

    c434 = fields.Float('434', readonly=False, )
    c444 = fields.Float('444', readonly=False, )
    c454 = fields.Float('454', readonly=False, )

    c480 = fields.Float('480', readonly=False, )
    c481 = fields.Float('481', readonly=False, )
    c482 = fields.Float('482', readonly=False, )
    c483 = fields.Float('483', readonly=False, )
    c484 = fields.Float('484', readonly=False, )
    c485 = fields.Float('485', readonly=False, )
    c499 = fields.Float('499', readonly=False, )

    c500 = fields.Float('500', readonly=False, )
    c510 = fields.Float('510', readonly=False, )
    c520 = fields.Float('520', readonly=False, )

    c501 = fields.Float('501', readonly=False, )
    c511 = fields.Float('511', readonly=False, )
    c521 = fields.Float('521', readonly=False, )

    c502 = fields.Float('502', readonly=False, )
    c512 = fields.Float('512', readonly=False, )
    c522 = fields.Float('522', readonly=False, )

    c503 = fields.Float('503', readonly=False, )
    c513 = fields.Float('513', readonly=False, )
    c523 = fields.Float('523', readonly=False, )

    c504 = fields.Float('504', readonly=False, )
    c514 = fields.Float('514', readonly=False, )
    c524 = fields.Float('524', readonly=False, )

    c505 = fields.Float('505', readonly=False, )
    c515 = fields.Float('515', readonly=False, )
    c525 = fields.Float('525', readonly=False, )

    c506 = fields.Float('506', readonly=False, )
    c516 = fields.Float('516', readonly=False, )

    c507 = fields.Float('507', readonly=False, )
    c517 = fields.Float('517', readonly=False, )

    c508 = fields.Float('508', readonly=False, )
    c518 = fields.Float('518', readonly=False, )

    c509 = fields.Float('509', readonly=False, )
    c519 = fields.Float('519', readonly=False, )
    c529 = fields.Float('529', readonly=False, )

    c531 = fields.Float('531', readonly=False, )
    c541 = fields.Float('541', readonly=False, )

    c532 = fields.Float('532', readonly=False, )
    c542 = fields.Float('542', readonly=False, )

    c543 = fields.Float('543', readonly=False, )

    c544 = fields.Float('544', readonly=False, )
    c554 = fields.Float('554', readonly=False, )

    c535 = fields.Float('535', readonly=False, )
    c545 = fields.Float('545', readonly=False, )
    c555 = fields.Float('555', readonly=False, )

    c563 = fields.Float('563', readonly=False, )
    c564 = fields.Float('564', readonly=False, )

    c601 = fields.Float('601', readonly=False, )
    c602 = fields.Float('602', readonly=False, )
    c605 = fields.Float('605', readonly=False, )
    c607 = fields.Float('607', readonly=False, )
    c609 = fields.Float('609', readonly=False, )
    c611 = fields.Float('611', readonly=False, )
    c612 = fields.Float('612', readonly=False, )
    c613 = fields.Float('613', readonly=False, )
    c615 = fields.Float('615', readonly=False, )
    c617 = fields.Float('617', readonly=False, )
    c619 = fields.Float('619', readonly=False, )
    c621 = fields.Float('621', readonly=False, )
    c699 = fields.Float('699', readonly=False, )

    c721 = fields.Float('721', readonly=False, )
    c723 = fields.Float('723', readonly=False, )
    c725 = fields.Float('725', readonly=False, )
    c727 = fields.Float('727', readonly=False, )
    c729 = fields.Float('729', readonly=False, )
    c731 = fields.Float('731', readonly=False, )

    c799 = fields.Float('799', readonly=False, )
    c859 = fields.Float('859', readonly=False, )

    c890 = fields.Float('890', )

    @api.multi
    @api.onchange('c102', 'c101')
    def _onchange_104_values(self):
        for r in self:

            # VENTAS

            # TODO Cambiar el codigo para formatear todos los campos excepto c101 y c102 en un solo paso para evitar errores.
            # Reinicia los valores a 0 al cambiar las fechas.
            r.c401 = 0.0
            r.c411 = 0.0
            r.c421 = 0.0
            r.c402 = 0.0
            r.c412 = 0.0
            r.c422 = 0.0
            r.c403 = 0.0
            r.c413 = 0.0
            r.c404 = 0.0
            r.c414 = 0.0
            r.c405 = 0.0
            r.c415 = 0.0
            r.c406 = 0.0
            r.c416 = 0.0
            r.c407 = 0.0
            r.c417 = 0.0
            r.c408 = 0.0
            r.c418 = 0.0
            r.c431 = 0.0
            r.c441 = 0.0
            r.c434 = 0.0
            r.c444 = 0.0
            r.c454 = 0.0

            r.c563 = 0.0
            r.c564 = 0.0

            r.c601 = 0.0
            r.c602 = 0.0
            r.c605 = 0.0
            r.c607 = 0.0
            r.c609 = 0.0
            r.c611 = 0.0
            r.c612 = 0.0
            r.c613 = 0.0
            r.c615 = 0.0
            r.c617 = 0.0
            r.c619 = 0.0
            r.c621 = 0.0
            r.c699 = 0.0

            sale_lines = self.env['account.invoice.line'].search([]).filtered(
                lambda l: str(l.invoice_id.date_invoice)[0:4] == r.c102 and
                          str(l.invoice_id.date_invoice)[5:7] == r.c101 and
                          l.invoice_id.type in ('out_invoice', 'out_refund') and
                          l.invoice_id.state in ('open', 'paid'))


            for line in sale_lines:
                # Evita la declaración de facturas sin comprobante válido.
                if not line.invoice_id.comprobante_id.code:
                    continue

                else:
                    tax_ids = line.invoice_line_tax_ids
                    for tax in tax_ids:
                        tax_amount = line.compute_inline_taxes(line, tax)
                        if line.invoice_id.type == 'out_invoice':
                            for tag in tax.tag_ids:
                                if tax.tax_group_id.name in ('ImpGrav', 'Imponible'):
                                    if tag.name == '104_401':
                                        r.c401 += line.price_subtotal
                                        r.c411 += line.price_subtotal
                                        r.c421 += tax_amount
                                    elif tag.name == '104_402':
                                        r.c402 += line.price_subtotal
                                        r.c412 += line.price_subtotal
                                        r.c422 += tax_amount
                                    elif tag.name == '104_403':
                                        r.c403 += line.price_subtotal
                                        r.c413 += line.price_subtotal
                                    elif tag.name == '104_404':
                                        r.c404 += line.price_subtotal
                                        r.c414 += line.price_subtotal
                                    elif tag.name == '104_405':
                                        r.c405 += line.price_subtotal
                                        r.c415 += line.price_subtotal
                                    elif tag.name == '104_406':
                                        r.c406 += line.price_subtotal
                                        r.c416 += line.price_subtotal
                                    elif tag.name == '104_407':
                                        r.c407 += line.price_subtotal
                                        r.c417 += line.price_subtotal
                                    elif tag.name == '104_408':
                                        r.c408 += line.price_subtotal
                                        r.c418 += line.price_subtotal
                                elif tax.tax_group_id.name in ('NoGraIva', 'ImpExe'):
                                    if tag.name == '104_431':
                                        r.c431 += line.price_subtotal
                                elif tax.tax_group_id.name in 'Intermediario':
                                    if tag.name == '104_434':
                                        r.c434 += line.price_subtotal
                                elif tax.tax_group_id.name in 'RetIva':
                                    if tag.name == '104_609':
                                        if not line.invoice_id.r_secuencial:
                                            continue
                                        else:
                                            r.c609 += tax_amount
                        elif line.invoice_id.type == 'out_refund':
                            for tag in tax.tag_ids:
                                if tax.tax_group_id.name in ('ImpGrav', 'Imponible'):
                                    if tag.name == '104_401':
                                        r.c411 -= line.price_subtotal
                                        r.c421 -= tax_amount
                                    elif tag.name == '104_402':
                                        r.c412 -= line.price_subtotal
                                        r.c422 -= tax_amount
                                    elif tag.name == '104_403':
                                        r.c413 -= line.price_subtotal
                                    elif tag.name == '104_404':
                                        r.c414 -= line.price_subtotal
                                    elif tag.name == '104_405':
                                        r.c415 -= line.price_subtotal
                                    elif tag.name == '104_406':
                                        r.c416 -= line.price_subtotal
                                    elif tag.name == '104_407':
                                        r.c417 -= line.price_subtotal
                                    elif tag.name == '104_408':
                                        r.c418 -= line.price_subtotal
                                elif tax.tax_group_id.name in ('NoGraIva', 'ImpExe'):
                                    if tag.name == '104_431':
                                        r.c431 -= line.price_subtotal
                                elif tax.tax_group_id.name in 'Intermediario':
                                    if tag.name == '104_434':
                                        r.c434 -= line.price_subtotal
                                elif tax.tax_group_id.name in 'RetIva':
                                    if tag.name == '104_609':
                                        if not line.invoice_id.r_secuencial:
                                            continue
                                        else:
                                            r.c609 -= tax_amount

            # COMPRAS

            # Reinicia los valores a 0 al cambiar las fechas.
            r.c500 = 0.0
            r.c510 = 0.0
            r.c520 = 0.0
            r.c501 = 0.0
            r.c511 = 0.0
            r.c521 = 0.0
            r.c502 = 0.0
            r.c512 = 0.0
            r.c522 = 0.0
            r.c503 = 0.0
            r.c513 = 0.0
            r.c523 = 0.0
            r.c504 = 0.0
            r.c514 = 0.0
            r.c524 = 0.0
            r.c505 = 0.0
            r.c515 = 0.0
            r.c525 = 0.0
            r.c506 = 0.0
            r.c516 = 0.0
            r.c507 = 0.0
            r.c517 = 0.0
            r.c508 = 0.0
            r.c518 = 0.0
            r.c531 = 0.0
            r.c541 = 0.0
            r.c532 = 0.0
            r.c542 = 0.0
            r.c543 = 0.0
            r.c544 = 0.0
            r.c554 = 0.0
            r.c535 = 0.0
            r.c545 = 0.0
            r.c555 = 0.0

            r.c799 = 0.0
            r.c859 = 0.0

            r.c890 = 0.0

            r.c721 = 0.0
            r.c723 = 0.0
            r.c725 = 0.0
            r.c727 = 0.0
            r.c729 = 0.0
            r.c731 = 0.0

            # Obtiene las lineas de compras 
            purchase_lines = self.env['account.invoice.line'].search([]).filtered(
                lambda l: str(l.invoice_id.date_invoice)[0:4] == r.c102 and
                          str(l.invoice_id.date_invoice)[5:7] == r.c101 and
                          l.invoice_id.type == 'in_invoice' and
                          l.invoice_id.state in ('open', 'paid'))

            for line in purchase_lines:
                if not line.invoice_id.comprobante_id.code:
                    continue

                else:
                    tax_ids = line.invoice_line_tax_ids
                    for tax in tax_ids:
                        tax_amount = line.compute_inline_taxes(line, tax)
                        if line.invoice_id.type == 'in_invoice':
                            for tag in tax.tag_ids:
                                if tax.tax_group_id.name in ('ImpGrav', 'Imponible'):
                                    if tag.name == '104_500':
                                        r.c500 += line.price_subtotal
                                        r.c510 += line.price_subtotal
                                        r.c520 += tax_amount
                                    elif tag.name == '104_501':
                                        r.c501 += line.price_subtotal
                                        r.c511 += line.price_subtotal
                                        r.c521 += tax_amount
                                    elif tag.name == '104_502':
                                        r.c502 += line.price_subtotal
                                        r.c512 += line.price_subtotal
                                        r.c522 += tax_amount
                                    elif tag.name == '104_503':
                                        r.c503 += line.price_subtotal
                                        r.c513 += line.price_subtotal
                                        r.c523 += tax_amount
                                    elif tag.name == '104_504':
                                        r.c504 += line.price_subtotal
                                        r.c514 += line.price_subtotal
                                        r.c524 += tax_amount
                                    elif tag.name == '104_505':
                                        r.c505 += line.price_subtotal
                                        r.c515 += line.price_subtotal
                                        r.c525 += tax_amount
                                    elif tag.name == '104_506':
                                        r.c506 += line.price_subtotal
                                        r.c516 += line.price_subtotal
                                    elif tag.name == '104_507':
                                        r.c507 += line.price_subtotal
                                        r.c517 += line.price_subtotal
                                    elif tag.name == '104_508':
                                        r.c508 += line.price_subtotal
                                        r.c510 += line.price_subtotal
                                elif tax.tax_group_id.name in ('NoGraIva', 'ImpExe'):
                                    if tag.name == '104_531':
                                        r.c531 += line.price_subtotal
                                        r.c541 += line.price_subtotal
                                    elif tag.name == '104_532':
                                        r.c532 += line.price_subtotal
                                        r.c542 += line.price_subtotal
                                elif tax.tax_group_id.name in 'Intermediario':
                                    if tag.name == '104_535':
                                        r.c535 += line.price_subtotal
                                        r.c545 += line.price_subtotal
                                        r.c555 += tax_amount
                                elif tax.tax_group_id.name in (
                                        'RetBien10', 'RetBienes', 'RetServ100', 'RetServ20', 'RetServicios'):
                                    if line.invoice_id.r_secuencial == 0:
                                        continue
                                    elif tag.name == '104_721':
                                        r.c721 += tax_amount
                                    elif tag.name == '104_723':
                                        r.c723 += tax_amount
                                    elif tag.name == '104_725':
                                        r.c725 += tax_amount
                                    elif tag.name == '104_727':
                                        r.c727 += tax_amount
                                    elif tag.name == '104_729':
                                        r.c729 += tax_amount
                                    elif tag.name == '104_731':
                                        r.c731 += tax_amount
                        elif line.invoice_id.type == 'in_refund':
                            for tag in tax.tag_ids:
                                if tax.tax_group_id.name in ('ImpGrav', 'Imponible'):
                                    if tag.name == '104_500':
                                        r.c510 -= line.price_subtotal
                                        r.c520 -= tax_amount
                                    elif tag.name == '104_501':
                                        r.c511 -= line.price_subtotal
                                        r.c521 -= tax_amount
                                    elif tag.name == '104_502':
                                        r.c512 -= line.price_subtotal
                                        r.c522 -= tax_amount
                                    elif tag.name == '104_503':
                                        r.c513 -= line.price_subtotal
                                        r.c523 -= tax_amount
                                    elif tag.name == '104_504':
                                        r.c514 -= line.price_subtotal
                                        r.c524 -= tax_amount
                                    elif tag.name == '104_505':
                                        r.c515 -= line.price_subtotal
                                        r.c525 -= tax_amount
                                    elif tag.name == '104_506':
                                        r.c516 -= line.price_subtotal
                                    elif tag.name == '104_507':
                                        r.c517 -= line.price_subtotal
                                    elif tag.name == '104_508':
                                        r.c510 -= line.price_subtotal
                                elif tax.tax_group_id.name in ('NoGraIva', 'ImpExe'):
                                    if tag.name == '104_531':
                                        r.c541 -= line.price_subtotal
                                    elif tag.name == '104_532':
                                        r.c542 -= line.price_subtotal
                                elif tax.tax_group_id.name in 'Intermediario':
                                    if tag.name == '104_535':
                                        r.c545 -= line.price_subtotal
                                        r.c555 -= tax_amount
                                elif tax.tax_group_id.name in (
                                        'RetBien10', 'RetBienes', 'RetServ100', 'RetServ20', 'RetServicios'):
                                    if not line.invoice_id.r_secuencial:
                                        continue
                                    elif tag.name == '104_721':
                                        r.c721 -= tax_amount
                                    elif tag.name == '104_723':
                                        r.c723 -= tax_amount
                                    elif tag.name == '104_725':
                                        r.c725 -= tax_amount
                                    elif tag.name == '104_727':
                                        r.c727 -= tax_amount
                                    elif tag.name == '104_729':
                                        r.c729 -= tax_amount
                                    elif tag.name == '104_731':
                                        r.c731 -= tax_amount

            r.c409 = r.c401 + r.c402 + r.c403 + r.c404 + r.c405 + r.c406 + r.c407 + r.c408
            r.c419 = r.c411 + r.c412 + r.c413 + r.c414 + r.c415 + r.c416 + r.c417 + r.c418
            r.c429 = r.c421 + r.c422

            r.c482 = r.c429
            r.c485 = r.c482 - r.c484
            r.c499 = r.c483 + r.c484

            r.c509 = r.c500 + r.c501 + r.c502 + r.c503 + r.c504 + r.c505 + r.c506 + r.c507 + r.c508
            r.c519 = r.c510 + r.c511 + r.c512 + r.c513 + r.c514 + r.c515 + r.c516 + r.c517 + r.c518
            r.c529 = r.c520 + r.c521 + r.c522 + r.c523 + r.c524 + r.c525

            if r.c419:
                r.c563 = (r.c411 + r.c412 + r.c415 + r.c416 + r.c417 + r.c418) / r.c419

            r.c564 = (r.c520 + r.c521 + r.c523 + r.c524 + r.c525) * r.c563

    @api.multi
    def print_f104(self):
        return self.env['report'].get_action(self, 'l10n_ec_sri.f104_report')
