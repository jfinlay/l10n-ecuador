# -*- coding: utf-8 -*-

from tempfile import mkdtemp
from collections import OrderedDict
import os
from os.path import basename
from shutil import move, rmtree
import re
import unicodedata
import zipfile
from openerp import models, fields, api, _


class py2xml():
    def __init__(self):
        self.data = ""

    def parse(self, pythonobj, objname, file):
        """
        processes Python data structure into XML string
        needs objName if pythonObj is a List
        """
        if pythonobj is None:
            return ""
        if isinstance(pythonobj, dict):
            self.data = self._dict2xml(pythonobj, objname, file)
        elif isinstance(pythonobj, list):
            self.data = self._list2xml(pythonobj, objname, file, True)
        else:
            self.data = "<{0}>{1}</{0}>".format(objname, str(pythonobj))
        return self.data

    def _dict2xml(self, pydictobj, objname, file):
        """
        process Python Dict objects
        They can store XML attributes and/or children
        """
        tagstr = ""
        attributes = {}
        attrstr = ""
        childstr = ""
        str_prop = ""
        if pydictobj:
            file.write("<{}>\n".format(objname))
            for k, v in pydictobj.items():
                if isinstance(v, dict):
                    childstr += self._dict2xml(v, k, file)
                elif isinstance(v, list):
                    childstr += self._list2xml(v, k, file, True)
                else:
                    if v:
                        str_prop = "<{0}>{1}</{0}>\n".format(k, v)
                        file.write(str_prop)
                    else:
                        str_prop = "<{}/>\n".format(k)
                        file.write(str_prop)
        file.write("</{}>\n".format(objname))
        return childstr

    def _list2xml(self, listobj, objname, file, op):
        """
        process Python List objects
        They have no attributes, just children
        Lists only hold Dicts or Strings
        """
        tagstr = ""
        childstr = ""
        for childobj in listobj:
            if isinstance(childobj, dict):
                childstr += self._dict2xml(childobj, objname[:-1], file)
            elif isinstance(childobj, list) and op is True:
                childstr += self._list2xml(childobj, objname[:-1], file, False)
            else:
                for string in childobj:
                    childstr += string
        if objname is None:
            return childstr
        tagstr += "<{0}>{1}</{0}>".format(objname, childstr)
        return tagstr


class Ats(models.Model):
    _name = "l10n_ec_sri.ats"
    _description = "Annex Transactional Simplified"

    # Definir el mes por defecto del reporte
    def _default_mes(self):
        mes = fields.datetime.now().strftime("%m")
        if mes == '01':
            default = '12'
        else:
            default = str(int(mes) - 1).zfill(2)
        return default

    # Definir el ano por defecto del reporte
    def _default_anio(self):
        mes = fields.datetime.now().strftime("%m")
        anio = fields.datetime.now().strftime("%Y")
        if mes == '01':
            default = str(int(anio) - 1)
        else:
            default = str(anio)
        return default

    def _default_company(self):
        return self.env.user.company_id.id

    @api.depends('mes', 'anio')
    def _ats_name(self):
        for rec in self:
            rec.name = rec.mes + '-' + rec.anio + '.zip'

    name = fields.Char('Annex', compute='_ats_name')
    mes = fields.Selection(
        [
            ('01', _('January')),
            ('02', _('February')),
            ('03', _('March')),
            ('04', _('April')),
            ('05', _('May')),
            ('06', _('June')),
            ('07', _('July')),
            ('08', _('August')),
            ('09', _('September')),
            ('10', _('October')),
            ('11', _('November')),
            ('12', _('December'))
        ],
        string='Mes',
        default=_default_mes,
        required=True
    )
    anio = fields.Char(
        'Año', default=_default_anio, required=True, )

    file_save = fields.Binary('XML File', readonly=True)
    company_id = fields.Many2one(
        'res.company',
        string=_('Company'),
        default=_default_company,
        readonly=True,
    )

    def remove_accents(self, s):
        return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))

    def date_convert(self, date_invoice):
        """
        date: '2012-12-15'
        return: '15/12/2012'
        """
        cdate = '00/00/0000'
        if date_invoice:
            f = date_invoice.split('-')
            cdate = '{}/{}/{}'.format(f[2], f[1], f[0])
        return cdate

    def _get_tax_amount(self, inv):
        tax = 0.0
        for i in inv.tax_line_ids:
            if i.amount > 0:
                tax += i.amount
        return tax

    def _get_base_amount(self, inv):
        data = {
            'baseNoGraIva': 0.0,
            'baseImponible': 0.0,
            'baseImpGrav': 0.0,
            'baseImpExe': 0.0,
        }
        for i in inv.invoice_line_ids:
            data['baseNoGraIva'] += i.basenograiva
            data['baseImponible'] += i.baseimponible
            data['baseImpGrav'] += i.baseimpgrav
            data['baseImpExe'] += i.baseimpexe
        return data

    def _get_retention_amount(self, inv):
        data = {
            'valRetBien10': 0.00,
            'valRetServ20': 0.00,
            'valorRetBienes': 0.00,
            'valRetServ50': 0.00,
            'valorRetServicios': 0.00,
            'valRetServ100': 0.00
        }
        for line in inv.invoice_line_ids:
            tax_ids = line.invoice_line_tax_ids
            for tax in tax_ids:
                tax_amount = line.compute_inline_taxes(line, tax)
                for tag in tax.tag_ids:
                    if tag.name == '104_721':
                        data['valRetBien10'] += abs(tax_amount)
                    elif tag.name == '104_723':
                        data['valRetServ20'] += abs(tax_amount)
                    elif tag.name == '104_725':
                        data['valorRetBienes'] += abs(tax_amount)
                    elif tag.name == '104_727':
                        data['valRetServ50'] += abs(tax_amount)
                    elif tag.name == '104_729':
                        data['valorRetServicios'] += abs(tax_amount)
                    elif tag.name == '104_731':
                        data['valRetServ100'] += abs(tax_amount)
        return data

    def _get_sustento(self, row):
        # TODO: Toma el primer sustento de la factura, debe hacer el proceso por cada sustento
        for line in row.invoice_line_ids:
            for tax in line.invoice_line_tax_ids:
                if tax.sustento_id:
                    return tax.sustento_id.name

    def process_lines(self, line):
        line_obj = self.env['account.invoice.line']
        tax_ids = line.invoice_line_tax_ids
        data = {}
        for tax in tax_ids:
            if tax.tax_group_id.name in 'RetIr':
                tax_amount = line_obj.compute_inline_taxes(line, tax)
                if abs(tax_amount) > 0:
                    data['codRetAir'] = tax.sri_code
                    data['baseImpAir'] = abs(line.price_subtotal)
                    data['porcentajeAir'] = abs(tax.amount)
                    data['valRetAir'] = abs(tax_amount)
        return data

    @api.multi
    def get_purchase_dict(self, main_id, purchase_ids, main_data):
        main_data[main_id] = {}
        if not main_data[main_id].get('purchase_ids', {}):
            main_data[main_id] = {'purchase_ids': []}
        for row in purchase_ids:
            ret_data = []
            purchase_res = OrderedDict()
            pagos_ext = OrderedDict()
            pagos_ext['pagoLocExt'] = '01'
            pagos_ext['paisEfecPago'] = 'NA'
            pagos_ext['aplicConvDobTrib'] = 'NA'
            pagos_ext['pagExtSujRetNorLeg'] = 'NA'

            # Solo las facturas que tienen autorizacion
            if row.comprobante_id.code:
                purchase_res['codSustento'] = self._get_sustento(row) or row.comprobante_id.code
                purchase_res['tpIdProv'] = row.partner_id.property_account_position_id.identificacion_id.tpidprov
                purchase_res['idProv'] = row.partner_id.vat_ec
                purchase_res['tipoComprobante'] = '{:0>2}'.format(row.comprobante_id.code)
                purchase_res['parteRel'] = 'NO'
                if row.partner_id.property_account_position_id.identificacion_id.code == 'P':
                    purchase_res['tipoProv'] = row.partner_id.property_account_position_id.persona_id.idprov
                purchase_res['fechaRegistro'] = self.date_convert(row.date_invoice)
                if row.comprobante_id.code == '03':
                    se = row.c_autorizacion_id.establecimiento
                    pe = row.c_autorizacion_id.puntoemision
                    sec = '{:0>9}'.format(row.secuencial or 0)
                    auth = row.c_autorizacion_id.autorizacion
                elif row.comprobante_id.code in ['11', '12', '19', '20']:
                    se = row.c_detercero_id.establecimiento or '999'
                    pe = row.c_detercero_id.puntoemision or '999'
                    sec = '{:0>9}'.format(row.secuencial or '999999999')
                    auth = '{:0>10}'.format(row.c_detercero_id.autorizacion or '999999999')
                else:
                    se = row.c_detercero_id.establecimiento
                    pe = row.c_detercero_id.puntoemision
                    sec = '{:0>9}'.format(row.secuencial or 0)
                    auth = row.c_detercero_id.autorizacion
                purchase_res['establecimiento'] = se
                purchase_res['puntoEmision'] = pe
                purchase_res['secuencial'] = sec
                purchase_res['fechaEmision'] = self.date_convert(row.date_invoice)
                purchase_res['autorizacion'] = auth
                base_data = self._get_base_amount(row)
                purchase_res['baseNoGraIva'] = '{0:.2f}'.format(base_data['baseNoGraIva'])
                purchase_res['baseImponible'] = '{0:.2f}'.format(base_data['baseImponible'])
                purchase_res['baseImpGrav'] = '{0:.2f}'.format(base_data['baseImpGrav'])
                purchase_res['baseImpExe'] = '{0:.2f}'.format(base_data['baseImpExe'])
                purchase_res['montoIce'] = '{0:.2f}'.format(0)
                purchase_res['montoIva'] = '{0:.2f}'.format(row.montoiva)
                ret_data = self._get_retention_amount(row)
                purchase_res['valRetBien10'] = '{0:.2f}'.format(ret_data['valRetBien10'])
                purchase_res['valRetServ20'] = '{0:.2f}'.format(ret_data['valRetServ20'])
                purchase_res['valorRetBienes'] = '{0:.2f}'.format(ret_data['valorRetBienes'])
                purchase_res['valRetServ50'] = '{0:.2f}'.format(ret_data['valRetServ50'])
                purchase_res['valorRetServicios'] = '{0:.2f}'.format(ret_data['valorRetServicios'])
                purchase_res['valRetServ100'] = '{0:.2f}'.format(ret_data['valRetServ100'])
                purchase_res['totbasesImpReemb'] = '{0:.2f}'.format(0)
                modificado = False
                if row.documento_modificado_id:
                    modificado = True
                    if row.documento_modificado_id.comprobante_id.code == '03':
                        dse = row.documento_modificado_id.c_autorizacion_id.establecimiento
                        dpe = row.documento_modificado_id.c_autorizacion_id.puntoemision
                        dsec = '{:0>9}'.format(row.documento_modificado_id.secuencial or 0)
                        dauth = row.documento_modificado_id.autorizacion
                    else:
                        dse = row.documento_modificado_id.c_detercero_id.establecimiento
                        dpe = row.documento_modificado_id.c_detercero_id.puntoemision
                        dsec = '{:0>9}'.format(row.documento_modificado_id.secuencial or 0)
                        dauth = row.documento_modificado_id.c_detercero_id.autorizacion
                    purchase_res['docModificado'] = '{:0>2}'.format(
                        int(row.documento_modificado_id.comprobante_id.code or 0))
                    purchase_res['estabModificado'] = '{:0>3}'.format(dse or 0)
                    purchase_res['ptoEmiModificado'] = '{:0>3}'.format(dpe or 0)
                    purchase_res['secModificado'] = '{:0>9}'.format(int(dsec or 0))
                    purchase_res['autModificado'] = dauth
                purchase_res['pagoExterior'] = pagos_ext
                retir = False
                ret_data = []
                for i in row.invoice_line_ids:
                    for j in i.invoice_line_tax_ids:
                        if j.tax_group_id.name == 'RetIr':
                            ret_data.append(j.id)
                if ret_data:
                    retir = True
                data_payment = self._get_base_amount(row)
                data_tax = self._get_tax_amount(row)
                payments = (sum(data_payment.values()) + data_tax) >= 1000
                if payments:
                    purchase_res['formasDePagos'] = []
                air = False
                if retir and not modificado:
                    air = True
                    purchase_res['airrr'] = []
                if retir and not modificado and row.r_autorizacion_id:
                    purchase_res['estabRetencion1'] = row.r_autorizacion_id.establecimiento or '000'
                    purchase_res['ptoEmiRetencion1'] = row.r_autorizacion_id.puntoemision or '000'
                    purchase_res['secRetencion1'] = '{:0>9}'.format(row.r_secuencial or 0)
                    purchase_res['autRetencion1'] = '{:0>10}'.format(row.r_autorizacion_id.autorizacion or 0)
                    purchase_res['fechaEmiRet1'] = self.date_convert(row.r_date)
                main_data[main_id]['purchase_ids'].append(purchase_res)
                purchase_index = main_data[main_id]['purchase_ids'].index(purchase_res)
                if row.payment_ids and payments:
                    if not main_data[main_id]['purchase_ids'][purchase_index].get('formasDePago', {}):
                        main_data[main_id]['purchase_ids'][purchase_index]['formasDePagos'] = []
                    for payment in row.payment_ids:
                        payment_res = {'formaPago': payment.formapago_id.code or '00'}
                        main_data[main_id]['purchase_ids'][purchase_index]['formasDePagos'].append(payment_res)
                if retir and air:
                    data_air = []
                    det_air = OrderedDict()
                    if not main_data[main_id]['purchase_ids'][purchase_index].get('airrr', {}):
                        main_data[main_id]['purchase_ids'][purchase_index]['airrr'] = []
                    for line in row.invoice_line_ids:
                        air = self.process_lines(line)
                        if air and not det_air.get(air['codRetAir'], False):
                            det_air[air['codRetAir']] = {
                                'codRetAir': air['codRetAir'],
                                'baseImpAir': 0,
                                'porcentajeAir': '',
                                'valRetAir': 0
                            }
                        if air:
                            det_air[air['codRetAir']]['codRetAir'] = air['codRetAir']
                            det_air[air['codRetAir']]['baseImpAir'] += float(air['baseImpAir'])
                            det_air[air['codRetAir']]['porcentajeAir'] = '{0:.0f}'.format(air['porcentajeAir'])
                            det_air[air['codRetAir']]['valRetAir'] += float(air['valRetAir'])
                    if det_air:
                        for x, y in det_air.items():
                            data_air.append({
                                'detalleAir': {
                                    'codRetAir': y['codRetAir'],
                                    'baseImpAir': '{0:.2f}'.format(y['baseImpAir']),
                                    'porcentajeAir': y['porcentajeAir'],
                                    'valRetAir': '{0:.2f}'.format(y['valRetAir']),
                                }
                            })
                        main_data[main_id]['purchase_ids'][purchase_index]['airrr'].append(data_air)
        return True

    def get_sale_dict(self, main_id, sale_ids, main_data, total_sales):
        pdata = {}
        if not main_data[main_id].get('sale_ids', {}):
            main_data[main_id]['sale_ids'] = []
        for row in sale_ids:
            # Solo las facturas que tienen autorizacion
            if not row.c_autorizacion_id: continue
            if not pdata or not pdata.get(row.partner_id.vat_ec, False):
                partner_data = {
                    row.partner_id.vat_ec: {
                        row.comprobante_id.code: {
                            'tpIdCliente': row.partner_id.property_account_position_id.identificacion_id.tpidcliente,
                            'idCliente': row.partner_id.vat_ec,
                            'tipoComprobante': row.comprobante_id.code,
                            'parteRelVtas': 'SI' if row.partner_id.parterel is True else 'NO',
                            'numeroComprobantes': 0,
                            'baseNoGraIva': 0,
                            'baseImponible': 0,
                            'baseImpGrav': 0,
                            'montoIva': 0,
                            'valorRetRenta': 0,
                            'valorRetIva': 0
                        }
                    }
                }
                pdata.update(partner_data)
            base_data = self._get_base_amount(row)
            pdata[row.partner_id.vat_ec][row.comprobante_id.code]['numeroComprobantes'] += 1
            pdata[row.partner_id.vat_ec][row.comprobante_id.code]['baseNoGraIva'] += base_data['baseNoGraIva']
            pdata[row.partner_id.vat_ec][row.comprobante_id.code]['baseImponible'] += base_data['baseImponible']
            pdata[row.partner_id.vat_ec][row.comprobante_id.code]['baseImpGrav'] += base_data['baseImpGrav']
            pdata[row.partner_id.vat_ec][row.comprobante_id.code]['montoIva'] += row.montoiva
            retir = False
            ret_data = []
            for i in row.invoice_line_ids:
                for j in i.invoice_line_tax_ids:
                    if j.tax_group_id.name == 'RetIr':
                        ret_data.append(j.id)
            if ret_data:
                retir = True
            if retir:
                for line in row.invoice_line_ids:
                    air = self.process_lines(line)
                    if air:
                        pdata[row.partner_id.vat_ec][row.comprobante_id.code]['valorRetRenta'] += air['valRetAir']
            vat_ret = self._get_retention_amount(row)
            pdata[row.partner_id.vat_ec][row.comprobante_id.code]['valorRetIva'] += abs(
                vat_ret['valorRetBienes']) + abs(vat_ret['valorRetServicios'])
        for i, j in pdata.items():
            for k, l in j.items():
                sale_res = OrderedDict()
                sale_res['tpIdCliente'] = l['tpIdCliente']
                sale_res['idCliente'] = l['idCliente']
                sale_res['parteRelVtas'] = l['parteRelVtas']
                sale_res['tipoComprobante'] = l['tipoComprobante']
                sale_res['numeroComprobantes'] = '{0:.0f}'.format(l['numeroComprobantes'])
                sale_res['baseNoGraIva'] = '{0:.2f}'.format(l['baseNoGraIva'])
                sale_res['baseImponible'] = '{0:.2f}'.format(l['baseImponible'])
                sale_res['baseImpGrav'] = '{0:.2f}'.format(l['baseImpGrav'])
                sale_res['montoIva'] = '{0:.2f}'.format(l['montoIva'])
                sale_res['valorRetIva'] = '{0:.2f}'.format(l['valorRetIva'])
                sale_res['valorRetRenta'] = '{0:.2f}'.format(l['valorRetRenta'])
                main_data[main_id]['sale_ids'].append(sale_res)
                if sale_res['tipoComprobante'] != '04':
                    total_sales[main_id]['baseNoGraIva'] += float(sale_res['baseNoGraIva'])
                    total_sales[main_id]['baseImponible'] += float(sale_res['baseImponible'])
                    total_sales[main_id]['baseImpGrav'] += float(sale_res['baseImpGrav'])
        return True

    def get_sale_refund_dict(self, main_id, sale_ids, main_data, total_refund):
        pdata = {}
        if not main_data[main_id].get('sale_ids', {}):
            main_data[main_id]['sale_ids'] = []
        for row in sale_ids:
            if not pdata or not pdata.get(row.partner_id.vat_ec, False):
                partner_data = {
                    row.partner_id.vat_ec: {
                        row.comprobante_id.code: {
                            'tpIdCliente': row.partner_id.property_account_position_id.identificacion_id.tpidcliente,
                            'idCliente': row.partner_id.vat_ec,
                            'tipoComprobante': row.comprobante_id.code,
                            'parteRelVtas': 'SI' if row.partner_id.parterel is True else 'NO',
                            'numeroComprobantes': 0,
                            'baseNoGraIva': 0,
                            'baseImponible': 0,
                            'baseImpGrav': 0,
                            'montoIva': 0,
                            'valorRetRenta': 0,
                            'valorRetIva': 0
                        }
                    }
                }
                pdata.update(partner_data)
            base_data = self._get_base_amount(row)
            pdata[row.partner_id.vat_ec][row.comprobante_id.code]['numeroComprobantes'] += 1
            pdata[row.partner_id.vat_ec][row.comprobante_id.code]['baseNoGraIva'] += base_data['baseNoGraIva']
            pdata[row.partner_id.vat_ec][row.comprobante_id.code]['baseImponible'] += base_data['baseImponible']
            pdata[row.partner_id.vat_ec][row.comprobante_id.code]['baseImpGrav'] += base_data['baseImpGrav']
            pdata[row.partner_id.vat_ec][row.comprobante_id.code]['montoIva'] += row.montoiva
            retir = False
            ret_data = []
            for i in row.invoice_line_ids:
                for j in i.invoice_line_tax_ids:
                    if j.tax_group_id.name == 'RetIr':
                        ret_data.append(j.id)
            if ret_data:
                retir = True
            if retir:
                for line in row.invoice_line_ids:
                    air = self.process_lines(line)
                    if air:
                        pdata[row.partner_id.vat_ec][row.comprobante_id.code]['valorRetRenta'] += air['valRetAir']
            vat_ret = self._get_retention_amount(row)
            pdata[row.partner_id.vat_ec][row.comprobante_id.code]['valorRetIva'] += abs(
                vat_ret['valorRetBienes']) + abs(vat_ret['valorRetServicios'])
        for i, j in pdata.items():
            for k, l in j.items():
                sale_res = OrderedDict()
                sale_res['tpIdCliente'] = l['tpIdCliente']
                sale_res['idCliente'] = l['idCliente']
                sale_res['parteRelVtas'] = l['parteRelVtas']
                sale_res['tipoComprobante'] = l['tipoComprobante']
                sale_res['numeroComprobantes'] = '{0:.0f}'.format(l['numeroComprobantes'])
                sale_res['baseNoGraIva'] = '{0:.2f}'.format(l['baseNoGraIva'])
                sale_res['baseImponible'] = '{0:.2f}'.format(l['baseImponible'])
                sale_res['baseImpGrav'] = '{0:.2f}'.format(l['baseImpGrav'])
                sale_res['montoIva'] = '{0:.2f}'.format(l['montoIva'])
                sale_res['valorRetIva'] = '{0:.2f}'.format(l['valorRetIva'])
                sale_res['valorRetRenta'] = '{0:.2f}'.format(l['valorRetRenta'])
                main_data[main_id]['sale_ids'].append(sale_res)
                if sale_res['tipoComprobante'] != '04':
                    total_refund[main_id]['baseNoGraIva'] += float(sale_res['baseNoGraIva'])
                    total_refund[main_id]['baseImponible'] += float(sale_res['baseImponible'])
                    total_refund[main_id]['baseImpGrav'] += float(sale_res['baseImpGrav'])
        return True

    def get_cancel_dict(self, main_id, canceled_ids, main_data):
        for row in canceled_ids:
            if not main_data[main_id].get('cancel_ids', {}):
                main_data[main_id]['cancel_ids'] = []
            cancel_res = OrderedDict()
            cancel_res['tipoComprobante'] = row.comprobante_id.code
            cancel_res['establecimiento'] = row.autorizacion_id.establecimiento
            cancel_res['puntoEmision'] = row.autorizacion_id.puntoemision
            cancel_res['secuencialInicio'] = row.secuencialinicio
            cancel_res['secuencialFin'] = row.secuencialfin
            cancel_res['autorizacion'] = row.autorizacion_id.autorizacion
            main_data[main_id]['cancel_ids'].append(cancel_res)
        return True

    @api.multi
    @api.depends('anio', 'mes')
    def create_xml(self):
        company_id = self.env.user.company_id.id
        total_sales = {}
        total_refund = {}
        for i in self:
            main = {}
            purchase_ids = self.env['account.invoice'].search([]).filtered(
                lambda r: str(r.date_invoice)[5:7] == self.mes and
                          str(r.date_invoice)[0:4] == self.anio and
                          r.type in ('in_invoice', 'in_refund') and
                          r.state in ('open', 'paid')
            )
            self.get_purchase_dict(self.id, purchase_ids, main)
            sale_ids = self.env['account.invoice'].search([]).filtered(
                lambda r: str(r.date_invoice)[5:7] == self.mes and
                          str(r.date_invoice)[0:4] == self.anio and
                          r.type == 'out_invoice' and r.comprobante_id.code != '04' and
                          r.state in ('open', 'paid')
            )
            total_sales[self.id] = {}
            total_sales[self.id]['baseNoGraIva'] = 0.0
            total_sales[self.id]['baseImponible'] = 0.0
            total_sales[self.id]['baseImpGrav'] = 0.0
            self.get_sale_dict(self.id, sale_ids, main, total_sales)
            sale_refund_ids = self.env['account.invoice'].search([]).filtered(
                lambda r: str(r.date_invoice)[5:7] == self.mes and
                          str(r.date_invoice)[0:4] == self.anio and
                          r.type == 'out_refund' and r.comprobante_id.code == '04' and
                          r.state in ('open', 'paid')
            )
            total_refund[self.id] = {}
            total_refund[self.id]['baseNoGraIva'] = 0.0
            total_refund[self.id]['baseImponible'] = 0.0
            total_refund[self.id]['baseImpGrav'] = 0.0
            self.get_sale_refund_dict(self.id, sale_refund_ids, main, total_refund)
            canceled_ids = self.env['l10n_ec_sri.comprobantesanulados'].search([]).filtered(
                lambda c: str(c.fecha)[5:7] == self.mes and str(c.fecha)[0:4] == self.anio
            )
            self.get_cancel_dict(self.id, canceled_ids, main)
            if main[self.id].get('sale_ids', {}):
                baseNoGraIva = total_sales[self.id]['baseNoGraIva']
                baseImponible = total_sales[self.id]['baseImponible']
                baseImpGrav = total_sales[self.id]['baseImpGrav']
                ref_baseNoGraIva = total_refund[self.id]['baseImpGrav']
                ref_baseImponible = total_refund[self.id]['baseImpGrav']
                ref_baseImpGrav = total_refund[self.id]['baseImpGrav']
            else:
                # En caso de no existir ventas
                baseNoGraIva = 0.0
                baseImponible = 0.0
                baseImpGrav = 0.0
                ref_baseNoGraIva = 0.0
                ref_baseImponible = 0.0
                ref_baseImpGrav = 0.0

            subtotal_ventas = (baseNoGraIva + baseImponible + baseImpGrav)
            subtotal_reemb = (ref_baseNoGraIva + ref_baseImponible + ref_baseImpGrav)
            total_ventas = subtotal_ventas - subtotal_reemb
            razon = re.sub(r'[\?|\$\.|\!]', r'', self.remove_accents(i.company_id.name))
            name_file = "{}-{}{}.xml".format("AT", self.mes, self.anio)
            try:
                import zlib
                mode = zipfile.ZIP_DEFLATED
            except ImportError:
                mode = zipfile.ZIP_STORED
            zip_name = "{}-{}{}.zip".format("AT", self.mes, self.anio)
            temp_folder = mkdtemp(prefix='ats_')
            named_file = open(os.path.join(temp_folder, name_file), 'wb')
            named_file.write('<?xml version="1.0" encoding="iso-8859-1"?>\n')
            named_file.write('<iva>\n')
            named_file.write('<TipoIDInformante>R</TipoIDInformante>\n')
            named_file.write('<IdInformante>{}</IdInformante>\n'.format(i.company_id.partner_id.vat_ec))
            named_file.write('<razonSocial>{}</razonSocial>\n'.format(razon))
            named_file.write('<Anio>{}</Anio>\n'.format(self.anio))
            named_file.write('<Mes>{}</Mes>\n'.format(self.mes))
            named_file.write('<numEstabRuc>{}</numEstabRuc>\n'.format(i.company_id.numestabruc))
            named_file.write('<totalVentas>{0:.2f}</totalVentas>\n'.format(total_ventas))
            named_file.write('<codigoOperativo>IVA</codigoOperativo>\n')
            serializer = py2xml()
            if main[self.id].get('purchase_ids', {}):
                named_file.write('<compras>\n')
                serializer.parse(main[self.id].get('purchase_ids', {}), 'detalleComprass', named_file)
                del main[self.id]['purchase_ids']
                named_file.write('</compras>\n')
            else:
                named_file.write('<compras/>\n')
            if main[self.id].get('sale_ids', {}):
                named_file.write("<ventas>\n")
                serializer.parse(main[self.id].get('sale_ids', {}), 'detalleVentass', named_file)
                del main[self.id]['sale_ids']
                named_file.write("</ventas>\n")
            else:
                named_file.write("<ventas/>\n")
            sales_total = {
                'codEstab': '{:0>3}'.format(i.company_id.numestabruc),
                'ventasEstab': '{0:.2f}'.format(total_ventas),
            }
            named_file.write("<ventasEstablecimiento>\n")
            serializer.parse(sales_total, 'ventaEst', named_file)
            named_file.write("</ventasEstablecimiento>\n")
            if main[self.id].get('cancel_ids', {}):
                named_file.write("<anulados>\n")
                serializer.parse(main[self.id].get('cancel_ids', {}), 'detalleAnuladoss', named_file)
                del main[self.id]['cancel_ids']
                named_file.write("</anulados>\n")
            else:
                named_file.write("<anulados/>\n")
            named_file.write('</iva>\n')
            named_file.close()
            findreplace = [
                (u'</formasDePago>\n<formasDePago>', u''),
                (u'</air>\n<air>', u''),
            ]

            def replaceStringInFile(filePath):
                # replaces all findStr by repStr in file filePath
                tempName = filePath + '~~~'
                inputFile = open(filePath)
                outputFile = open(tempName, 'w')
                fContent = unicode(inputFile.read())
                for aPair in findreplace:
                    outputText = fContent.replace(aPair[0], aPair[1])
                    fContent = outputText
                outputFile.write(outputText.encode("utf-8"))
                outputFile.close()
                inputFile.close()
                move(tempName, filePath)

            def dropempty(filepath):
                import fileinput
                for lines in fileinput.FileInput(filepath, inplace=1):
                    lines = lines.strip()
                    if lines == '':
                        continue
                        # print lines

            def topretty(filePath):
                tempName = filePath + '~~~'
                a = 'xmllint --format {} > {}'.format(filePath, tempName)
                os.popen(a, 'r', -1)
                move(tempName, filePath)

            def fileFilter(dummyArg, thisDir, dirChildrenList):
                for thisChild in dirChildrenList:
                    if os.path.splitext(thisChild)[1] in ['.xml'] and os.path.isfile(thisDir + '/' + thisChild):
                        replaceStringInFile(thisDir + '/' + thisChild)
                        # dropempty(thisDir + '/' + thisChild)
                        # topretty(thisDir + '/' + thisChild)

            # Causa vaciado del archivo en windows
            os.path.walk(temp_folder, fileFilter, None)
            zipf = zipfile.ZipFile(os.path.join(temp_folder, zip_name), 'w', mode)
            zipf.write(os.path.join(temp_folder, name_file), basename(name_file))
            zipf.close()
            out_zip = open(os.path.join(temp_folder, zip_name), "rb").read().encode("base64")
            self.write({'name': zip_name, 'file_save': out_zip})
            rmtree(temp_folder)
        return True
