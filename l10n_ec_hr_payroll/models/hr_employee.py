# -*- coding: utf-8 -*-
from datetime import date, datetime

from openerp import _, api, fields, models
from openerp.exceptions import ValidationError
from stdnum.ec import vat


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    @api.multi
    def _check_old_age(self):
        for row in self:
            if row.birthday:
                row.old_age = False
                date_birthday = datetime.strptime(row.birthday, "%Y-%m-%d")
                if date_birthday <= datetime.today():
                    today = datetime.today().strftime("%Y-%m-%d")
                    now = today.split('-')
                    birth = row.birthday.split('-')
                    date_now = date(int(now[0]), int(now[1]), int(now[2]))
                    date_birth = date(int(birth[0]), int(birth[1]), int(birth[2]))
                    delta = date_now - date_birth
                    age = delta.days / 365
                    limit = 65 if row.gender == 'male' else 60
                    if age >= limit:
                        row.old_age = True

    flag = fields.Boolean(_('Job Change'))
    old_age = fields.Boolean(
        compute=_check_old_age, string=("Old Age"),
        store=False, oldname='tercera_edad'
    )
    department_id = fields.Many2one('hr.department', string=_(
        'Department'), related='job_id.department_id', readonly=True)
    parent_id = fields.Many2one('hr.employee', string=_(
        'Manager'), related='department_id.manager_id', readonly=True)

    @api.constrains('identification_id', 'passport_id')
    def _check_identification_number(self):
        for row in self:
            if row.identification_id is False and row.passport_id is False:
                raise ValidationError(
                    _('The employee must have an identification number or passport number'))

            identification_ids = [i.id for i in self.search([('identification_id', '=', row.identification_id),
                                                             ('identification_id', '!=', False)])]
            if identification_ids and row.id not in identification_ids:
                raise ValidationError(_('The identification number must be unique per employee'))
            passport_ids = [i.id for i in self.search([('passport_id', '=', row.passport_id),
                                                       ('passport_id', '!=', False)])]
            if passport_ids and row.id not in passport_ids:
                raise ValidationError(_('The identification number must be unique per employee'))
            if row.identification_id and not vat.ci.is_valid(row.identification_id):
                raise ValidationError(_('The identification number is wrong'))

    @api.model
    def create(self, vals):
        if vals.get('job_id'):
            vals['flag'] = True
        return super(HrEmployee, self).create(vals)

    @api.multi
    def write(self, vals):
        if vals.get('job_id'):
            vals['flag'] = True
        return super(HrEmployee, self).write(vals)
