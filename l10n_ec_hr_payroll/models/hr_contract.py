# -*- coding: utf-8 -*-

from datetime import datetime

from openerp import _, api, exceptions, fields, models
from openerp.exceptions import UserError


class HrContract(models.Model):
    _inherit = 'hr.contract'

    @api.onchange('basic_wage')
    def _onchange_basic_wage(self):
        if self.basic_wage is True:
            self.wage = self.env.user.company_id.basic_wage
        else:
            self.wage = 0.0

    flag = fields.Boolean(_('Contract Change'))
    active = fields.Boolean(_('Active Contract'), default=True)
    department_id = fields.Many2one('hr.department', string=_(
        'Department'), related='employee_id.department_id', readonly=True)
    job_id = fields.Many2one('hr.job', string=_(
        'Job Title'), related='employee_id.job_id', readonly=True)
    fondos_reserva_rol = fields.Boolean('Pagar fondos de reserva en rol', )
    decimo_tercero_rol = fields.Boolean('Pagar décimo tercero en rol', )
    decimo_cuarto_rol = fields.Boolean('Pagar décimo cuarto en rol', )
    gratificacion = fields.Float('Gratificación ($)', )
    provisionar_vacaciones = fields.Boolean('Provisionar vacaciones mensualmente', )
    impuesto_renta = fields.Float('Impuesto a la renta a pagar ($)', )  # TODO: Calcular
    basic_wage = fields.Boolean(_('Basic Wage'), oldname='sueldo_basico')
    hour_cost = fields.Float(_('Hour Cost'), compute='_get_hour_cost', store=True)
    prestamos = fields.One2many(
        'hr.contract.prestamo', 'hr_contract_id',
        string='Préstamos / Adelantos', )
    projection_ids = fields.One2many(
        'hr.annual.projection',
        'contract_id',
        string='Proyección de gastos personales',
    )
    rent_tax_ids = fields.One2many(
        'hr.annual.rent.tax',
        'contract_id',
        string='Impuesto a la renta',
    )
    hist_job_ids = fields.One2many('hr.contract.job', 'contract_id', string=_('Job History'))
    hist_wage_ids = fields.One2many('hr.contract.wage', 'contract_id', string=_('Wage History'))

    @api.model
    def create(self, vals):
        if vals.get('wage'):
            vals['flag'] = True
        return super(HrContract, self).create(vals)

    @api.multi
    def write(self, vals):
        if vals.get('wage'):
            vals['flag'] = True
        return super(HrContract, self).write(vals)

    @api.multi
    @api.depends('wage')
    def _get_hour_cost(self):
        for row in self:
            row.hour_cost = row.wage / 240

    @api.constrains('employee_id', 'active')
    def _check_active_contract(self):
        for row in self:
            contract_ids = [x.id for x in self.search(
                [('employee_id', '=', row.employee_id.id), ('active', '=', True)])]
            contract_ids.remove(row.id)
            if len(contract_ids) >= 1:
                raise UserError(_('¡You can only register one active contract per employee!'))

    @api.constrains('wage')
    def _check_wage(self):
        for row in self:
            if not row.wage or row.wage == 0:
                raise UserError(_('¡You can not save a contract with salary equal to zero!'))


class HrContractJob(models.Model):
    """
    Historical record of job changes
    """
    _name = 'hr.contract.job'
    _description = __doc__

    name = fields.Char(_('Reason'))
    contract_id = fields.Many2one('hr.contract', string=_('Contract'))
    old_job_id = fields.Many2one('hr.job', string=_('Old Job'))
    job_id = fields.Many2one('hr.job', string=_('Job'))
    date = fields.Date(_('Update Date'))


class HrContractWage(models.Model):
    """
    Historical record of wage changes
    """
    _name = 'hr.contract.wage'
    _description = __doc__

    name = fields.Char(_('Reason'))
    contract_id = fields.Many2one('hr.contract', string=_('Contract'))
    old_wage = fields.Float(_('Old Wage'))
    wage = fields.Float(_('Wage'))
    date = fields.Date(_('Update Date'))


class hr_contract_prestamo(models.Model):
    _name = 'hr.contract.prestamo'
    _description = 'Prestamos y adelantos'
    _order = 'termina_pago,state desc'

    hr_contract_id = fields.Many2one('hr.contract', string='Contract', required=True)
    type = fields.Selection(
        [
            ('prestamo', 'Prestamos'),
            ('adelanto', 'Adelanto')
        ],
        'Type', required=True)
    subtype = fields.Selection(
        [
            ('quirografario', 'Quirografario'),
            ('hipotecario', 'Hipotecario')
        ],
        'Subtype')
    monto = fields.Float('Monto recurrente', required=True)
    inicia_pago = fields.Date('Pagar desde', required=True)
    termina_pago = fields.Date('Pagar hasta', required=True)
    state = fields.Selection(
        [
            ('draft', 'Borrador'),
            ('run', 'En ejecutar'),
            ('paid', 'Pagado'),
            ('cancel', 'Cancelado'),
        ],
        'Estado', default='draft')
    debit_account_id = fields.Many2one('account.account', string='Cuenta de debito')
    credit_account_id = fields.Many2one('account.account', string='Cuenta de crédito')
    journal_id = fields.Many2one('account.journal', string='Diario')
    move_id = fields.Many2one('account.move', string='Movimiento')
    create_move = fields.Boolean('Crear movimiento')

    @api.multi
    def action_draft(self):
        self.state = 'draft'

    @api.multi
    def action_run(self):
        if self.type == 'prestamo' or (self.type == 'adelanto' and not self.create_move):
            self.state = 'run'
            return {}

        move_pool = self.env['account.move']
        timenow = datetime.now().strftime('%Y-%m-%d')
        default_partner_id = self.hr_contract_id.employee_id.address_id.id
        name = 'Adelanto a %s (%s)' % (self.hr_contract_id.employee_id.name, timenow)
        move = {
            'narration': name,
            'date': timenow,
            'ref': name,
            'journal_id': self.journal_id.id,
        }

        line_ids = []

        if self.debit_account_id:
            debit_line = (0, 0, {
                'name': name,
                'date': timenow,
                'partner_id': default_partner_id or False,
                'account_id': self.debit_account_id.id,
                'journal_id': self.journal_id.id,
                'debit': self.monto > 0.0 and self.monto or 0.0,
                'credit': self.monto < 0.0 and -self.monto or 0.0,
            })
            line_ids.append(debit_line)

        if self.credit_account_id:
            credit_line = (0, 0, {
                'name': name,
                'date': timenow,
                'partner_id': default_partner_id or False,
                'account_id': self.debit_account_id.id,
                'journal_id': self.journal_id.id,
                'debit': self.monto < 0.0 and -self.monto or 0.0,
                'credit': self.monto > 0.0 and self.monto or 0.0,
            })
            line_ids.append(credit_line)

        move.update({'line_id': line_ids})
        move_id = move_pool.create(move)
        self.move_id = move_id
        move_pool.post([move_id])
        self.state = 'run'

    @api.multi
    def action_paid(self):
        self.state = 'paid'

    @api.multi
    def action_cancel(self):
        move_obj = self.pool.get('account.move')
        try:
            move_obj.button_cancel(self._cr, self._uid, self.move_id.id, context=self._context)
        except:
            raise exceptions.ValidationError("El asiento relacionado a este adelanto no puede ser cancelado, "
                                             "primero reverselo.")
        self.state = 'cancel'


class hr_sri_retencion(models.Model):
    _name = 'hr.sri.retencion'
    _description = 'Tabla de retenciones de I.R.'
    _order = 'name asc'

    def _default_year(self):
        month = fields.datetime.now().strftime("%m")
        year = fields.datetime.now().strftime("%Y")
        if month == '01':
            default = str(int(year) - 1)
        else:
            default = str(year)
        return default

    def _default_name(self):
        month = fields.datetime.now().strftime("%m")
        year = fields.datetime.now().strftime("%Y")
        if month == '01':
            default = str(int(year) - 1)
        else:
            default = str(year)
        name_reference = '{} {}'.format('Tabla de retención', default)
        return name_reference

    name = fields.Char(string='Descripción', required=True, default=_default_name)
    year = fields.Selection(
        [
            ('2016', '2016'),
            ('2017', '2017'),
        ],
        string='Año',
        default=_default_year,
        required=True
    )
    active = fields.Boolean(string='Activo', default=True)
    line_ids = fields.One2many('hr.sri.retencion.line', 'retencion_id', string='Detalle')
    projection_line_ids = fields.One2many('hr.projection', 'retencion_id', string='Proyecciones')
    maximo_deducible = fields.Float(string='Max. Deducible', default=0.0)


class hr_sri_retencion_line(models.Model):
    _name = 'hr.sri.retencion.line'
    _description = 'Línea de retención de I.R.'
    _rec_name = 'fraccion_basica'
    _order = 'fraccion_basica asc'

    fraccion_basica = fields.Float(string='Fracción básica', required=True, default=0.0)
    exceso_hasta = fields.Float(string='Exceso hasta', required=True, default=0.0)
    impuesto_fraccion_basica = fields.Float(
        string='Impuesto fracción básica', required=True, default=0.0)
    porcentaje = fields.Float(string='% fracción excedente', required=True, default=0.0)
    retencion_id = fields.Many2one('hr.sri.retencion', string='Detalle',
                                   ondelete='cascade', default=0.0)


class hr_deduction(models.Model):
    _name = 'hr.deduction'
    _description = 'Deductions Taxes for Rent'
    _order = 'name asc, code desc'

    code = fields.Char(string='Código', required=True, size=8)
    name = fields.Char(string='Nombre', required=True, size=32)
    description = fields.Text('Descripción')


class hr_projection(models.Model):
    _name = 'hr.projection'
    _description = 'Max Deduction Value'
    _order = 'name asc'

    name = fields.Many2one('hr.deduction', string='Deducción', required=True)
    valor_maximo = fields.Float(string='Máximo deducible', required=True)
    retencion_id = fields.Many2one('hr.sri.retencion', string='Detalle', ondelete='cascade')


class hr_annual_projection(models.Model):
    _name = 'hr.annual.projection'
    _description = 'Annual Projection'

    def _default_year(self):
        month = fields.datetime.now().strftime("%m")
        year = fields.datetime.now().strftime("%Y")
        if month == '01':
            default = str(int(year) - 1)
        else:
            default = str(year)
        return default

    def _default_name(self):
        month = fields.datetime.now().strftime("%m")
        year = fields.datetime.now().strftime("%Y")
        if month == '01':
            default = str(int(year) - 1)
        else:
            default = str(year)
        name_reference = '{} {}'.format('Proyección de gastos', default)
        return name_reference

    @api.multi
    def _compute_all(self):
        for i in self:
            sum = 0.0
            for line in i.line_ids:
                sum += line.value
            self.total = sum

    name = fields.Char(string='Descripción', default=_default_name)
    year = fields.Selection(
        [
            ('2016', '2016'),
            ('2017', '2017'),
        ],
        string='Año',
        default=_default_year,
        required=True
    )
    total = fields.Float(compute=_compute_all, method=True, string='Total')
    line_ids = fields.One2many('hr.annual.projection.line',
                               'projection_id', string='Líneas de proyección')
    contract_id = fields.Many2one('hr.contract', string='Contrato')


class hr_annual_projection_line(models.Model):
    _name = 'hr.annual.projection.line'

    name = fields.Char(string='Nombre', size=8)
    deduccion_id = fields.Many2one('hr.deduction', string='Deducción')
    value = fields.Float(string='Valor')
    projection_id = fields.Many2one('hr.annual.projection', string='Año')


class hr_sri_exoneration(models.Model):
    _name = 'hr.sri.exoneration'
    _description = 'Exoneration Table for Taxes Rent'
    _order = 'name asc'

    def _default_year(self):
        month = fields.datetime.now().strftime("%m")
        year = fields.datetime.now().strftime("%Y")
        if month == '01':
            default = str(int(year) - 1)
        else:
            default = str(year)
        return default

    def _default_name(self):
        month = fields.datetime.now().strftime("%m")
        year = fields.datetime.now().strftime("%Y")
        if month == '01':
            default = str(int(year) - 1)
        else:
            default = str(year)
        name_reference = '{} {}'.format('Tabla de exoneración', default)
        return name_reference

    name = fields.Char(string='Descripción', required=True, default=_default_name)
    tercera_edad = fields.Float(string='Exoneración tercera edad')
    year = fields.Selection(
        [
            ('2016', '2016'),
            ('2017', '2017'),
        ],
        string='Año',
        default=_default_year,
        required=True
    )
    active = fields.Boolean(string='Activo', default=True)
    line_ids = fields.One2many('hr.sri.exoneration.line', 'exoneration_id', string='Detalle')


class hr_sri_exoneration_line(models.Model):
    _name = 'hr.sri.exoneration.line'
    _description = 'Exoneration Table detail for Taxes Rent'
    _order = 'exceso_desde asc'

    exceso_desde = fields.Float(string='Exceso desde', required=True)
    exceso_hasta = fields.Float(string='Exceso hasta', required=True)
    porcentaje = fields.Float(string='% Exoneración', required=True)
    exoneration_id = fields.Many2one('hr.sri.exoneration', string='Exoneración', ondelete='cascade')


class hr_annual_rent_tax(models.Model):
    _name = 'hr.annual.rent.tax'

    name = fields.Char(string='Descripción')
    year = fields.Selection(
        [
            ('2016', '2016'),
            ('2017', '2017'),
        ],
        string='Año',
    )
    line_ids = fields.One2many('hr.rent.tax', 'rent_id', string='Impuesto a la renta')
    contract_id = fields.Many2one('hr.contract', string='Contrato')


class hr_rent_tax(models.Model):
    _name = 'hr.rent.tax'
    _order = 'year, month asc'

    month = fields.Selection(
        [
            ('01', 'Enero'),
            ('02', 'Febrero'),
            ('03', 'Marzo'),
            ('04', 'Abril'),
            ('05', 'Mayo'),
            ('06', 'Junio'),
            ('07', 'Julio'),
            ('08', 'Agosto'),
            ('09', 'Septiembre'),
            ('10', 'Octubre'),
            ('11', 'Noviembre'),
            ('12', 'Diciembre')
        ],
        string='Mes',
        required=True
    )
    year = fields.Selection(
        [
            ('2016', '2016'),
            ('2017', '2017'),
        ],
        string='Año',
        required=True
    )
    aportes_proyectables = fields.Float(string='Renuneración unificada (Proyectable)')
    aportes_no_proyectables = fields.Float(string='Remuneración no proyectable')
    value = fields.Float(string='I.R. retenido')
    rent_id = fields.Many2one('hr.annual.rent.tax', string='Tipo de retención')
