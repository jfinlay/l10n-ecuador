# -*- coding: utf-8 -*-
from openerp import models, fields, api


class AccountInvoice(models.Model):
    _inherit = ['account.invoice']

    invoice_ats_errors = fields.Char(
        'Validaciones ATS',
        compute="_invoice_checker",
        store=True, )

    @api.multi
    @api.depends('state', 'r_secuencial', 'r_date', 'comprobante_id',
                 'invoice_line_ids', 'date_invoice', 'partner_id', 'total')
    def _invoice_checker(self):
        retenciones = ('RetIr', 'RetIva', 'RetBien10', 'RetBienes', 'RetServ100', 'RetServ20', 'RetServicios')
        impuestos = ('ImpExe', 'ImpGrav', 'Imponible', 'Intermediario', 'NoGraIva',)

        for inv in self:
            errors = ''
            if inv.state != 'cancelled':
                # VALIDACIONES DEL TIPO DE COMPROBANTE

                if not inv.comprobante_id.code:
                    if inv.tax_line_ids:
                        errors += 'El documento registra impuestos pero no tiene un comprobante válido. '
                    else:
                        errors += 'Este documento no se considerará en sus declaraciones. '

                # VALIDACIONES DE LA RETENCIÓN
                # Verifica si el comprobante tiene retención.
                if any(tax.tax_id.tax_group_id.name in retenciones for tax in inv.tax_line_ids
                       ):
                    if not inv.r_comprobante_id.code:
                        errors += 'Ingrese un comprobante de retención. '
                    if len(str(inv.r_secuencial)) > 9:
                        errors += 'El secuencial debe tener menos de 10 caracteres. '
                    # Si tiene retenciones debe haber un registro con la base
                    for line in inv.invoice_line_ids:
                        if not any(tax.tax_group_id.name in impuestos for tax in line.invoice_line_tax_ids):
                            errors += 'Si registra una retención debe registrar el código de la base imponible. '

                    # Validar la fecha de retención.
                    if inv.r_date:
                        if inv.date_invoice > inv.r_date:
                            errors += '[RFI]La fecha del comprobante de retención' \
                                      ' debe ser mayor a la fecha del comprobante. '
            # Valida datos del partner
            if not inv.partner_id.vat_ec:
                errors += 'El tercero no tiene registrado ruc o cédula. '
            if not inv.partner_id.property_account_position_id:
                errors += 'El tercero no tiene registrado el tipo de contribuyente. '

            # Valida autorización
            if inv.c_detercero_id:
                if len(inv.c_detercero_id.establecimiento) != 3:
                    errors += 'Existe un error en la autorización el establecimiento es incorrecto. '
                if len(inv.c_detercero_id.puntoemision) != 3:
                    errors += 'Existe un error en la autorización el punto de impresión es incorrecto. '
                if int(inv.secuencial) == 0:
                    errors += 'El secuencial de la factura es incorrecto. '

            if inv.c_autorizacion_id:
                if len(inv.c_autorizacion_id.establecimiento) != 3:
                    errors += 'Existe un error en la autorización el establecimiento es incorrecto. '
                if len(inv.c_autorizacion_id.puntoemision) != 3:
                    errors += 'Existe un error en la autorización el punto de impresión es incorrecto. '
                if int(inv.secuencial) == 0:
                    errors += 'El secuencial de la factura es 0. '

            if inv.r_autorizacion_id:
                if len(inv.r_autorizacion_id.establecimiento) != 3:
                    errors += 'Existe un error en la autorización de la retención el establecimiento es incorrecto. '
                if len(inv.r_autorizacion_id.puntoemision) != 3:
                    errors += 'Existe un error en la autorización de la retención el punto de impresión es incorrecto. '
                if int(inv.r_secuencial) == 0:
                    errors += 'El secuencial de la retención es 0. '

            inv.invoice_ats_errors = errors
